<?php
	$absolute_path_to_root = $_SERVER['DOCUMENT_ROOT'] . '/';
	$relative_path_to_root = '../';
	$nav_highlighted_page = 'insurance';
	$unique_page_id = 'insurance';
	require_once("{$absolute_path_to_root}templating.php");
	open_document();
	echo_head_section(array(
		'title' => 'Insurance:',
		'description' => 'Information on insurance coverage as it relates to treatment at the Vein Care Institute.',
		'keywords' => 'vein care, varicose veins, endovenous laser therapy, spider veins, vascular surgery, sclerotherapy'
	));
?>
<body class="page_<?php echo($unique_page_id); ?>">
	<?php echo_section('masthead'); ?>
	<?php echo_section('nav_menu'); ?>
	<?php echo_section('resources_rollout', array('location' => 'top')); ?>
	<?php echo_section('forms_rollout', array('location' => 'top')); ?>
	<?php echo_section('static_photo_header'); ?>
	<?php echo_section('nav_strip'); ?>
	<?php echo_section('forms_rollout', array('location' => 'side')); ?>
	<?php echo_section('resources_rollout', array('location' => 'side')); ?>
	<section class="gray bottom_layer first">
		<div class="width_limiter">
			<div class="block_container">
				<h2 class="full">Insurance Overview</h2>
				<div class="half">
					<p>One of the most frustrating aspects of healthcare, for patients and doctors, is dealing with insurance companies. This is especially true when it comes to vein disease. It is often thought that treatment for vein disease is always considered cosmetic, so it will not be covered by your insurance. In fact, more than 80% of what we do at VCI is covered by health insurance. Before we can ask your insurance for approval for your treatments, we have to verify and clearly document that you have a medical problem that is causing you symptoms and can be treated. To prove this, we do an ultrasound of the veins in your legs to see whether or not they are working correctly. If the veins in your legs are not working correctly, you may have a disease called <em>venous insufficiency</em>, which we can treat, and when treated will improve your symptoms. After proving that you have a problem we can fix, and documenting the problem and your symptoms, we send that documentation to your insurance company and request that they provide us with pre-authorization to treat you. Before your insurance company will approve your treatment, they usually want you</p>
				</div>
				<div class="half">
					<p>to try some things on your own, like wearing compression stockings and taking over-the-counter pain medicines like Advil, Aleve or ibuprofen. These requirements vary with each insurance plan, but we will be happy to help you learn what your plan requires.</p>
					<p>Once we receive approval from your insurance company for your treatment, we will contact you to schedule your treatment. Even if your insurance company agrees that your treatment is medically necessary and they will cover the treatment, you still may have some out-of-pocket costs. These costs differ greatly with every plan. Sometimes your out-of-pocket cost is just your specialist office visit copayment. Sometimes your out-of-pocket cost is a combination of your deductible and co-insurance amount. Before we schedule you for your treatment, we will make sure that you have the information that you need to determine what your out-of-pocket costs will be. We can give you the necessary information to help you contact your insurance company and and arrange a payment plan if necessary.</p>
				</div>
			</div>
		</div>
	</section>
	<section class="white top_layer">
		<div class="width_limiter">
			<div class="block_container">
				<h2 class="full long_top">Payment &amp; Insurance</h2>
				<div class="half">
					<h3 class="arrow gap_after">Out-of-Pocket Costs</h3>
					<p class="long_bottom">Your out-of-pocket costs are based on your contract with your insurance company. These costs can vary depending on what types of services you are receiving, whether or not you are seeing an in-network provider, and where you are receiving services. Prior to having any treatment at VCI, we will contact your insurance company and obtain preauthorization for your procedure. We can also give you the information needed for you to contact your insurer to find out what your out-of-pocket costs will be. We will provide you with the necessary information you will need to obtain your out-of-pocket cost information.</p>
				</div>
				<h3 class="half arrow gap_after">Some Insurance Plans We Accept</h3>
				<ul class="quarter textual">
					<li><a href="http://www.aarp.org/" target="_blank">AARP</a></li>
					<li><a href="http://www.anthem.com/" target="_blank">Anthem Blue Cross Blue Shield</a></li>
					<li><a href="http://www.aetna.com/" target="_blank">Aetna</a></li>
					<li><a href="http://www.beechstreet.com/" target="_blank">Beechstreet</a></li>
					<li><a href="http://www.cigna.com/" target="_blank">Cigna</a></li>
					<li><a href="http://www.cofinity.net/" target="_blank">Cofinity</a></li>
					<li><a href="http://coventryhealthcare.com/" target="_blank">Coventry</a></li>
					<li><a href="http://firsthealth.coventryhealthcare.com/" target="_blank">Firsthealth</a></li>
					<li><a href="http://www.geha.com/" target="_blank">GEHA</a></li>
					<li><a href="http://www.goldenrule.com/" target="_blank">Golden Rule</a></li>
					<li><a href="http://www.greatwesthealthcare.com/" target="_blank">Great West Healthcare</a></li>
				</ul>
				<ul class="quarter textual">
					<li><a href="http://www.anthem.com/wps/portal/ahpfooter?content_path=shared/noapplication/f0/s0/t0/pw_ad092390.htm&label=HMO%20Colorado&ref=universityofcolorado" target="_blank">HMO Colorado</a></li>
					<li><a href="http://www.humana.com/" target="_blank">Humana</a></li>
					<li><a href="http://www.medicare.gov/" target="_blank">Medicare</a></li>
					<li><a href="http://www.mutualofomaha.com/" target="_blank">Mutual of Omaha</a></li>
					<li><a href="http://www.pacificare.com/" target="_blank">Pacificare</a></li>
					<li><a href="http://www.phcs.com/" target="_blank">PHCS</a></li>
					<li><a href="http://www.rmhp.org/" target="_blank">Rocky Mountain Health Plans</a></li>
					<li><a href="http://www.securehorizons.com/" target="_blank">Secure Horizons</a></li>
					<li><a href="http://www.aetna.com/" target="_blank">Sloans Lake</a></li>
					<li><a href="http://www.tricare.mil/" target="_blank">Tricare</a></li>
					<li><a href="http://www.uhc.com/" target="_blank">United Healthcare</a></li>
				</ul>
				<p class="half" style="float: right;">If you don't see your plan on this list, please contact our office.</p>
			</div>
		</div>
	</section>
	<section class="gray bottom_layer">
		<div class="width_limiter">
			<div class="block_container">
				<h2 class="full long_top">Vocabulary</h2>
				<div class="half">
					<p>Getting your insurance to approve your procedure is half the battle. Understanding your coverage is the other half. There is nothing more frustrating than receiving information from your insurance that you don't understand. There are some key terms that your insurance uses that can be confusing. We will try to explain some of those terms for you now so that you can better understand your coverage and responsibilities through your insurance plan.</p>
					<h3>REFERRAL</h3>
					<p>A referral is an authorization from your insurance company for you to see a specialist. A referral has to be requested by your primary care doctor and submitted to your insurance company. Referrals are not always required for you to see a specialist; it depends on your insurance plan. Insurance plans that require referrals are Secure Horizons and all HMO plans.</p>
					<h3>CO-PAYMENT</h3>
					<p>A co-payment is an amount that your insurance has assigned you to pay when you have a medical appointment. The amount of your co-payment may be different depending on what kind of appointment you have. Your co-payment at your primary care doctor may be different than your copayment at a specialist's office. Your co-payment amounts are listed on your insurance card, and are due at the time you have your appointment.</p>
					<h3>DEDUCTIBLE</h3>
					<p>Your deductible is the amount of money that you have to pay out of pocket before your insurance will start to pay. Deductible amounts are different with every insurance plan. Also, some types of appointments don't apply to your deductible, meaning that you don't have to meet your deductible before your insurance will pay for them.</p>
				</div>
				<div class="half">
					<h3>CO-INSURANCE</h3>
					<p>Even after you meet your deductible, some plans require you to pay a co-insurance. The co-insurance amount is usually a percentage that your insurance company requires you to pay for every claim that they pay on. For example, if you have a 10% co-insurance and your insurance company gets a claim for $100.00, they will pay $90.00 and you will pay $10.00.</p>
					<h3>OUT-OF-POCKET MAXIMUM OR COST-SHARING CAP</h3>
					<p>This is the maximum amount of money you have to pay out of your pocket per year. This amount does not usually include your deductible or co-payments, but does include your co-insurance payments. After you reach your out-of-pocket maximum, your insurance covers your claims at 100%, so you no longer have to pay co-insurance amounts.</p>
					<h3>IN-NETWORK BENEFITS</h3>
					<p>Your insurance company makes contracts with doctors and facilities to provide care at lower costs. If you see a doctor, or go to a facility that has a contract with your insurance, you are going to an in-network provider. This means that your costs are reduced because your insurance company's costs are reduced. It is in your best interest to go to an in-network provider. Either your insurance company or the provider you want to see can let you know whether or not they are in your insurance company's network.</p>
					<h3>OUT-OF-NETWORK BENEFITS</h3>
					<p class="long_bottom">Some insurance plans do not offer out of network benefits, so you will want to check with your insurance company before you go to a provider that is not contracted with your insurance. If your insurance does provide out-of-network benefits, you will usually end up having more out-of-pocket costs because out of network benefits usually have a higher deductible and higher co-insurance amounts.</p>
				</div>
			</div>
		</div>
	</section>
	<?php echo_section('footer'); ?>
</body>
<?php close_document();