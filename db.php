<?php
require_once('config.php');
class DatabaseController {
	private $db;
	private $mode = 'pdo';
	private $allowed_modes = array('pdo', 'mysqli');
	public function __construct() {
		if (func_num_args() > 0) {
			$mode = func_get_arg(0);
			$this->set_mode($mode);
		}
		$this->init_db();
	}
	private function get_mode() {
		return $this->mode;
	}
	private function set_mode($mode) {
		if (in_array($mode, $this->allowed_modes)) {
			$this->mode = $mode;
		} else {
			trigger_error('Invalid mode specified for DatabaseController::set_mode(); "mysqli" assumed', E_USER_WARNING);
			$this->mode = 'mysqli';
		}
	}
	private function init_db() {
		global $FYL_GLOBALS;
		switch($this->mode) {
			case 'pdo': {
				try {
					$this->db = new PDO("mysql:host={$FYL_GLOBALS['database_server']};dbname={$FYL_GLOBALS['database_name']}", $FYL_GLOBALS['database_username'], $FYL_GLOBALS['database_password']);
					$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				} catch(PDOException $e) {
					echo "Error: " . $e->getMessage();
				}
				break;
			}
			case 'mysqli': {
				$this->db = new mysqli($FYL_GLOBALS['database_server'], $FYL_GLOBALS['database_username'], $FYL_GLOBALS['database_password'], $FYL_GLOBALS['database_name']);
				if ($this->db->connect_errno) {
					$errors[] = 'Could not connect to database.';
				}
				break;
			}
		}
	}
	public function db() {
		return $this->db;
	}
}
