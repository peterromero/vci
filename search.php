<?php
	$absolute_path_to_root = $_SERVER['DOCUMENT_ROOT'] . '/';
	$relative_path_to_root = './';
	$nav_highlighted_page = '';
	$unique_page_id = 'home';
	require_once("{$absolute_path_to_root}templating.php");
	require_once("{$absolute_path_to_root}functions.php");
	open_document();
	echo_head_section(array(
		'title' => 'Search Results:'
	));
	$q = '';
	if (array_key_exists('q', $_GET)) {
		if ($_GET['q'] != '') {
			$q = $_GET['q'];
		}
	}
?>
<body class="page_<?php echo($unique_page_id); ?>">
	<?php echo_section('masthead'); ?>
	<?php echo_section('nav_menu'); ?>
	<?php echo_section('resources_rollout', array('location' => 'top')); ?>
	<?php echo_section('nav_strip'); ?>
	<?php echo_section('resources_rollout', array('location' => 'side')); ?>
	<section class="gray bottom_layer first">
		<div class="width_limiter">
			<div class="block_container">
				<input type="text" name="search" class="half search on_light_bg" placeholder="Search" value="<?php echo($q); ?>">
				<?php
				if ($q) {
					$search_results = search($q);
					if (!$search_results) $search_results = 'Your search did not return any results.';
					echo("
						<h2 class=\"full\">Search Results</h2>
						<div class=\"full long_bottom\">$search_results</div>
					");
				} else {
					echo("
						<h2 class=\"full\">Search Results</h2>
						<div class=\"full long_bottom\">Please enter the text you wish to search for in the field above.</div>
					");
				}
				?>
			</div>
		</div>
	</section>
	<?php echo_section('footer'); ?>
</body>
<?php close_document();