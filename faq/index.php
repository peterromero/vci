<?php
	$absolute_path_to_root = $_SERVER['DOCUMENT_ROOT'] . '/';
	$relative_path_to_root = '../';
	$nav_highlighted_page = 'faq';
	$unique_page_id = 'faq';
	require_once("{$absolute_path_to_root}templating.php");
	open_document();
	echo_head_section(array(
		'title' => 'FAQ:',
		'description' => 'Frequently asked questions regarding vein therapy at the Vein Care Institute.',
		'keywords' => 'vein care, varicose veins, endovenous laser therapy, spider veins, vascular surgery, sclerotherapy'
	));
?>
<body class="page_<?php echo($unique_page_id); ?>">
	<?php echo_section('masthead'); ?>
	<?php echo_section('nav_menu'); ?>
	<?php echo_section('resources_rollout', array('location' => 'top')); ?>
	<?php echo_section('forms_rollout', array('location' => 'top')); ?>
	<?php echo_section('static_photo_header'); ?>
	<?php echo_section('nav_strip'); ?>
	<?php echo_section('forms_rollout', array('location' => 'side')); ?>
	<?php echo_section('resources_rollout', array('location' => 'side')); ?>
	<section class="gray bottom_layer first">
		<div class="width_limiter">
			<div class="block_container">
				<h2 class="full">Varicose Vein FAQ</h2>
				<div class="half">
					<h3>WHAT ARE VARICOSE VEINS?</h3>
					<p>Varicose veins are enlarged veins that become dilated as a result of faulty valves within the vein. This can lead to venous reflux or venous insufficiency. These valves are important because they help the legs pump blood back to the heart. If the valves fail, blood return out of the leg is hampered. This means that the blood in these veins tends to flow where gravity tells it to flow. This can cause the veins to become dilated and painful. In addition, the venous circulation (blood returning to the heart) is ineffective and there is a recirculation of this blood. This blood contains many waste products of metabolism, such as lactic acid. It is known that this recirculation phenomenon can lead to symptoms associated with varicose veins. The most common symptoms include burning, itching, tingling, fatigue, heaviness, cramping, aching, swelling, edema, and restlessness. Left untreated, these diseased veins can cause more serious medical conditions including skin inflammation, skin ulcers, vein inflammation, bleeding and blood clots.</p>
					<h3>WHAT CAUSES VARICOSE VEINS?</h3>
					<p class="long_bottom">Heredity is the number one factor causing varicose veins. You are twice as likely to have venous reflux (disease of the vein valves) if one of your parents had the condition. An inherited weakness of the vein wall and vein valves can cause enlargement and dilation of normal veins. Eventually they become large and symptomatic varicose veins. Women are more likely to suffer from varicose veins. In fact, women tend to have the disease at least twice as often as men. Men are not spared from the disease, however; in fact, about 25% of men between the ages of 40–49 have the disease. Other factors in women, such as pregnancy and hormonal factors, can also worsen the condition. In addition to family history and female gender, other aggravating factors predisposing to development of varicose veins include advancing age, obesity, trauma, intense weight-training exercise, and occupations involving prolonged sitting or standing.</p>
				</div>
				<div class="half">
					<h3>WOULDN'T CLOSING VARICOSE VEINS JUST REDIRECT FLOW INTO NORMAL VEINS AND MAKE THEM ALSO BECOME VARICOSE?</h3>
					<p>Since varicose veins have abnormal blood flow, the surrounding veins are already being burdened by having to compensate. Treatment of abnormal veins actually removes this burden from the surrounding veins, thereby improving the venous circulation. This improvement in venous circulation accounts for the improvement in varicose vein symptoms following effective varicose vein treatment. This is because the body redirects the blood flow into stronger, healthier, deeper veins within the legs.</p>
					<h3>DOES YOUR BODY NEED ALL OF ITS VEINS?</h3>
					<p>Basically, these veins are abnormal and diseased. Even when the veins are healthy they are expendable. We use healthy veins for bypass surgery regularly with few to no side effects. The symptoms of venous insufficiency are caused by the diseased veins. By treating the diseased veins, your symptoms disappear. These diseased veins actually put a burden on the healthy veins in your leg, causing them to work harder due to the inefficient circulation caused by the disease. There is really no down side to treatng these diseased veins and, in fact, this is the first step to restoring your circulation back to its original healthy state.</p>
					<h3>WHAT HAPPENS IF I NEED BYPASS SURGERY IN THE FUTURE, AND THE SAPHENOUS VEIN HAS BEEN REMOVED?</h3>
					<p>By definition, these veins are diseased and would not be used for any sort of bypass anyway. Other veins or grafts would have to be used even if the diseased vein was still present.</p>
					<h3>DOES MY INSURANCE COVER THE TREATMENT?</h3>
					<p>In the majority of cases the answer is yes. Venous insufficiency is recognized as a disease. As the treatments have improved and our knowledge of the disease has continued to grow in phenomenal ways, insurance companies are willing to pay for these low-risk, very successful treatments.</p>
				</div>
			</div>
		</div>
	</section>
	<section class="white top_layer">
		<div class="width_limiter">
			<div class="block_container">
				<h2 class="full long_top">Ultrasound Evaluation FAQ</h2>
				<div class="half">
					<h3>WHY IS IT IMPORTANT FOR YOU TO HAVE A VENOUS ULTRASOUND EVALUATION?</h3>
					<p class="long_bottom">In assessing symptomatic varicose veins, an expertly performed venous ultrasound evaluation by a specially trained ultrasound technician plays a critical role. A venous duplex ultrasound evaluation is performed which permits the surgeon to see the anatomy and to map out the underlying leg veins. Useful information is gathered to diagnose your specific vein problem. After the surgeon makes an accurate diagnosis, an appropriate and effective customized treatment plan can be developed for you. At the Vein Care Institute we perform this procedure in our office during your initial visit. Because the ultrasound is immediately reviewed by the physician, we can give you a detailed treatment plan and answer all of your questions at the first visit.</p>
				</div>
				<div class="half">
					<h3>WHAT IS AN ULTRASOUND EVALUATION LIKE?</h3>
					<p>After applying ultrasound gel to the leg, a handheld device is passed back and forth to evaluate the underlying veins. Sound waves sent into the leg bounce back to the handheld device whereby the attached ultrasound machine displays them as a picture on a monitor. The ultrasound evaluation is painless and usually takes between 30 and 45 minutes to complete.</p>
				</div>
			</div>
		</div>
	</section>
	<section class="gray bottom_layer">
		<div class="width_limiter">
			<div class="block_container">
				<h2 class="full long_top">Endovenous Thermal Ablation FAQ</h2>
				<div class="half">
					<h3>WHAT IS ENDOVENOUS THERMAL ABLATION (EVTA)?</h3>
					<p class="long_bottom">At VCI the endovenous thermal ablation procedure is performed by a vascular surgeon. The EVTA procedure is done in a procedure room at our office and requires only local anesthesia. Some patients prefer to have an oral sedative such as Valium or Xanax, and this can be given just prior to the procedure (if you opt to take the oral sedative, you must have someone to drive you home). A very thin bare laser fiber or radiofrequency catheter is inserted into the diseased vein within the leg through a tiny skin puncture. Laser or radiofrequency energy heats up the vein, causing it to shrink and collapse. The venous blood flow is then routed to healthy veins within the leg muscles.</p>
				</div>
				<div class="half">
					<p>Any residual branch veins remaining after the EVLA procedure are effectively treated by the phlebologist using ultrasound-guided sclerotherapy and/or visual sclerotherapy during follow up treatment sessions. Unlike surgical vein stripping where there is significant down time, patients treated with the EVLA procedure and/or with sclerotherapy have no down time, and can resume normal activities immediately.</p>
					<h3>CAN YOU EXERCISE AFTER THE EVTA PROCEDURE?</h3>
					<p class="long_bottom">Exercise is not only permitted following the EVTA procedure, it is encouraged. Immediately after the EVTA procedure, our patients are encouraged to walk for 20 minutes. We find that patients who use their leg muscles every day after the EVTA procedure have faster healing, less discomfort, and fewer complications. The simplest and most effective form of exercise to optimize leg vein function is walking. We often have our patients abstain from swimming and bathing (showering is okay) for 5 days following the EVTA procedure.</p>
				</div>
			</div>
		</div>
	</section>
	<?php echo_section('footer'); ?>
</body>
<?php close_document();