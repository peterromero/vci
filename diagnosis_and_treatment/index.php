<?php
	$absolute_path_to_root = $_SERVER['DOCUMENT_ROOT'] . '/';
	$relative_path_to_root = '../';
	$nav_highlighted_page = 'diagnosis';
	$unique_page_id = 'diagnosis';
	require_once("{$absolute_path_to_root}templating.php");
	open_document();
	echo_head_section(array(
		'title' => 'Diagnosis &amp; Treatment:',
		'description' => 'How does the Vein Care Institute diagnose and treat vein conditions?',
		'keywords' => 'vein care, varicose veins, endovenous laser therapy, spider veins, vascular surgery, sclerotherapy'
	));
?>
<body class="page_<?php echo($unique_page_id); ?>">
	<?php echo_section('masthead'); ?>
	<?php echo_section('nav_menu'); ?>
	<?php echo_section('resources_rollout', array('location' => 'top')); ?>
	<?php echo_section('forms_rollout', array('location' => 'top')); ?>
	<?php echo_section('static_photo_header'); ?>
	<?php echo_section('nav_strip'); ?>
	<?php echo_section('forms_rollout', array('location' => 'side')); ?>
	<?php echo_section('resources_rollout', array('location' => 'side')); ?>
	<section class="gray bottom_layer first">
		<div class="width_limiter">
			<div class="block_container">
				<h2 class="full">Non-Invasive Evaluation</h2>
				<div class="half">
					<p class="long_bottom">To find out exactly what's causing your problem, we take a look at your history and perform a focused physical examination. We'll listen very carefully to your description of symptoms and history with vein disease to see if there's a particular pattern of vein disease. The physical examination of the legs gives us a great deal of information and answers several key questions such as:</p>
					<ul class="textual italic indented">
						<li>Is this a vein-related disorder or another problem (arterial disease for example)?</li>
						<li>If this is venous insufficiency, are the deep veins involved?</li>
						<li>What patterns of reflux and varicose vein development are at work here? If the patient has spider veins, is this the only problem or is this just the outer manifestation of a deeper problem?</li>
						<li>What is producing this patient's symptoms?</li>
					</ul>
				</div>
				<p class="half">Initially, a venous duplex ultrasound evaluation is performed. This permits the vascular surgeon to see the anatomy and to map out the underlying leg veins. The ultrasound evaluation is painless and usually takes between 30 and 45 minutes to complete. In this time useful information is gathered to diagnose your specific vein problem. After the vascular surgeon makes an accurate diagnosis, an appropriate and effective customized treatment plan can be developed for you. At the Vein Care Institute we perform this procedure in our office during your initial visit and we can give you an immediate idea of the treatment plan and answer all of your questions.</p>
                <p class="half">We offer both endovenous laser and radiofrequency ablation. No sutures are required. Patients are awake and communicating with our surgeons during the procedure to assure they are comfortable. We believe general anesthesia is not only unnecessary but also contraindicated. A patient's ability to communicate any symptoms with the team during the procedure is essential and assures less discomfort after the procedure. Patients essentially return to normal daily activities the same day. Generally speaking, patients can plan to be at VCI for two hours the day of their procedure to allow ample time for registration and preparation. However, most procedures take on average only 45 minutes in our outpatient treatment facility.</p>
			</div>
		</div>
	</section>
	<section class="white top_layer">
		<div class="width_limiter">
			<div class="block_container">
				<h2 class="full long_top">Details &amp; Treatment Options</h2>
				<div class="third">
					<h3 class="arrow">Venous Insufficiency</h3>
					<a class="rollout_toggle_clicker" data-rollout-id="venous_insufficiency" href="#"><img alt="Venous Insufficiency" class="fill" src="/images/venous_insufficiency.jpg"></a>
					<p><span class="leader small">Legs ache and feel tired:</span>Everyone experiences aches and pains after a long day on their feet, but do your legs ache even when you haven't been on your feet much?</p>
					<p><span class="leader small">Legs feel "restless" with a need to keep them moving:</span>Sporadic fidgeting is natural, but do you feel a constant need to shake your feet or wiggle your legs, even when you're trying to go to sleep?</p>
					<p><span class="leader small">Legs swell or feel "heavy":</span>Fluctuations from occasional water retention are normal, but do your legs often seem unnaturally swollen? Does it sometimes feel like your shoes are filled with lead?</p>
					<a class="button rollout_trigger closer hide_above_single_column long_bottom" data-rollout-id="venous_insufficiency" data-toggle-text="Less Details" href="#">More Details</a>
				</div>
				<div class="third">
					<h3 class="arrow">Varicose Veins</h3>
					<a class="rollout_toggle_clicker" data-rollout-id="varicose_veins" href="#"><img alt="Varicose Veins" class="fill" src="/images/varicose_veins.jpg"></a>
					<p><span class="leader small">Veins appear dark purple or blue, twisted or bulging:</span>Legs sometimes resemble a road map, only one that likely won't take you anywhere fun.</p>
					<p><span class="leader small">Legs swell or feel "heavy":</span>Again, fluctuations from occasional water retention are normal, but if you're taken aback by unnatural looking legs, a red flag should go up.</p>
					<p><span class="leader small">Burning, throbbing and cramping may occur in lower legs:</span>Do you find yourself noticing frequently throughout the day that your legs just generally ache?</p>
					<a class="button third rollout_trigger closer hide_above_single_column long_bottom" data-rollout-id="varicose_veins" data-toggle-text="Less Details" href="#">More Details</a>
				</div>
				<div class="third">
					<h3 class="arrow">Spider Veins</h3>
					<a class="rollout_toggle_clicker" data-rollout-id="spider_veins" href="#"><img alt="Spider Veins" class="fill" src="/images/spider_veins.jpg"></a>
					<p>With pure spider veins, physical symptoms are usually milder or not present at all. If symptoms arise, these include:</p>
					<ul class="textual italic indented">
						<li>Tenderness directly over the spider vein</li>
						<li>Itching or burning at or around the spider veins</li>
						<li>Heat or dull ache in the area of the spider veins</li>
					</ul>
					<p>The primary symptoms of spider veins are mainly aesthetic and are characterized by small abnormal veins that are usually red, purple or blue.</p>
					<a class="button third rollout_trigger closer hide_above_single_column long_bottom" data-rollout-id="spider_veins" data-toggle-text="Less Details" href="#">More Details</a>
				</div>
			</div>
			<div class="block_container hide_below_full_width" style="margin-bottom: 48px;">
				<a class="button third rollout_trigger opener" data-rollout-id="venous_insufficiency" data-toggle-text="Less Details" href="#">More Details</a>
				<a class="button third rollout_trigger opener" data-rollout-id="varicose_veins" data-toggle-text="Less Details" href="#">More Details</a>
				<a class="button third rollout_trigger opener" data-rollout-id="spider_veins" data-toggle-text="Less Details" href="#">More Details</a>
			</div>
		</div>
	</section>
	<section class="gray bottom_layer rollout_content" data-rollout-id="venous_insufficiency">
		<div class="width_limiter">
			<div class="block_container">
				<h2 class="full long_top">Venous Insufficiency<a href="#" class="rollout_close" title="Close Details"></a></h2>
				<div class="full video_area left_description">
					<div class="description">
						<h3>Just the Facts</h3>
						<p>Venous insufficiency is the most severe form of vein disease. It can lead to changes in skin color, dermatitis, swelling, scar tissue formation in the skin and even the fat below the skin, as well as the formation of actual ulcers in the skin. Chronic venous insufficiency tends to affect the lower half of the leg, especially the tissues near the ankle.</p>
					</div>
					<div class="video">
						<iframe src="//player.vimeo.com/video/18900036?title=0&amp;byline=0&amp;portrait=0&amp;color=00AEC5" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
				</div>
				<h2 class="full">Venous Insufficiency Treatment Options</h2>
				<div class="two-thirds">
					<h3 class="arrow gap_after">Endovenous Laser Ablation</h3>
					<p>EVLA is a procedure that safely and effectively cures venous insufficiency. By treating the disease process, the symptoms of venous disease — including varicose veins — disappear. Unlike painful varicose vein surgery of the past, EVLA vein treatment shuts down veins from the inside by use of laser and ultrasound technology — all with minimal discomfort. Our initial assessment will include an ultrasonic evaluation of your veins. In real time our experts will make an accurate diagnosis, and an appropriate and effective customized treatment plan can be developed for you. EVLA involves entering the vein and treating the diseased vein from within. Using cutting edge ultrasound technology, the diseased vein is identified and entered using real-time ultrasound guidance.</p>
					<p>A small catheter (or IV) is inserted into the vein. This catheter is then advanced within the vein to treat the entire length of diseased vein. At this point, local anesthesia (also called <em>tumescent anesthesia</em>) is injected around the vein. This solution of local anesthesia serves two purposes. First, the vein itself as well as the surrounding tissues are anesthetized and become numb. Secondly, the fluid around the vein helps to protect the surrounding tissues from any collateral injury from the heat of the laser. Once the anesthesia has been injected around the vein, the laser is activated and the vein causing the patient's symptoms is completely ablated. Long-term follow-up of patients treated in this fashion indicate that less than 5% of patients have a recurrence of their disease.</p>
				</div>
				<div class="third">
					<h3 class="arrow gap_after">Recovery</h3>
					<p>Unlike surgical vein stripping where there is significant down time, patients treated with the EVLA procedure and/or with sclerotherapy usually have no down time, and can normally resume normal activities immediately. Exercise is not only permitted following the EVLA procedure, it is encouraged. Immediately after the EVLA procedure, we encourage patients to walk for 20 minutes. We find that patients who use their leg muscles every day after the EVLA procedure have faster healing, less discomfort, and fewer complications. The simplest and most effective form of exercise to optimize leg vein function is walking. We often have our patients abstain from swimming and bathing (showering is okay) for 5 days following the EVLA procedure.</p>
				</div>
				<div class="two-thirds">
					<h3 class="arrow gap_after">Radiofrequency Ablation</h3>
					<p>Radiofrequency ablation (RFA) is a minimally invasive procedure which uses segmental radiofrequency energy to provide an even and uniform heat to contract the collagen in the vein walls, causing them to collapse and seal the vein closed. Blood is then re-routed through healthy veins back towards the heart.</p>
					<p>The procedure involves inserting a catheter into the diseased vein through a small incision below the knee and threading the catheter through the vessel. The catheter delivers energy to the diseased vessel causing it to collapse. Blood flow is then naturally redirected to healthy veins as the recovery process begins. The treated vein becomes scar tissue and is eventually absorbed by the body.</p>
					<p>Based on certain anatomic considerations, your surgeon will select either EVLA or RFA as the energy source to use in closing your abnormal vein.</p>
				</div>
				<div class="third">
					<h3 class="arrow gap_after">Recovery</h3>
					<p>Unlike surgical vein stripping where there is significant down time, patients treated with the RFA procedure and/or with sclerotherapy usually have no down time, and can normally resume normal activities immediately. Exercise is not only permitted following the RFA procedure, it is encouraged. Immediately after the RFA procedure, we encourage patients to walk for 20 minutes. We find that patients who use their leg muscles every day after the RFA procedure have faster healing, less discomfort, and fewer complications. The simplest and most effective form of exercise to optimize leg vein function is walking. We often have our patients abstain from swimming and bathing (showering is okay) for 5 days following the RFA procedure.</p>
				</div>
				<div class="two-thirds">
					<h3 class="arrow gap_after">Sclerotherapy</h3>
					<p>Sclerotherapy involves the injection of a chemical agent into diseased veins. These veins typically are the smaller spider veins or slightly bigger reticular veins and even some larger varicose veins. The agent is injected directly into the vein and causes damage to the lining of the vein, which eventually causes the vein to completely disappear. Depending on the size of the vein, the solution can be agitated causing it to foam prior to injection. Foam sclerotherapy is very effective for some of the larger vessels.</p>
					<p>We use only the latest agents for injection — those that have been clinically proven to be effective. We have carefully chosen agents that are very low-risk and have extremely high efficacy. In addition, we adjust the concentration of the medication based on each individual patient. The procedure is essentially pain-free. We use extremely small needles for the injections. Most patients do not feel the needle enter the vein. The solution itself does not cause any discomfort and the procedure is typically nearly painless. Typically, the veins that are visible represent the tip of the iceberg. There are usually veins slightly deeper and somewhat bigger that are the actual source of the spider veins that are visible. Rather than simply injecting the visible veins, we strive to address the source of the problem. This not only results in much better long-term results; the cosmetic benefit is improved as well. Usually this requires the use of the vein viewer. This allows us to see those veins not visible to the naked eye. These veins tend to be the actual culprit and if they are not treated, the end result will be sub-optimal.</p>
				</div>
				<div class="third">
					<h3 class="arrow gap_after">Recovery</h3>
					<p>After the procedure there are no restrictions and any activities can be resumed.</p>
				</div>
				<h2 class="full">The Results</h2>
				<div class="half before_after">
					<img alt="Before venous insufficiency treatment" src="/images/venous_insufficiency_before.png">
					<h3>Before Treatment</h3>
				</div>
				<div class="half before_after">
					<img alt="After venous insufficiency treatment" src="/images/venous_insufficiency_after.png">
					<h3>After Treatment</h3>
				</div>
			</div>
		</div>
	</section>
	<section class="gray bottom_layer rollout_content" data-rollout-id="varicose_veins">
		<div class="width_limiter">
			<div class="block_container">
				<h2 class="full long_top">Varicose Veins<a href="#" class="rollout_close" title="Close Details"></a></h2>
				<div class="full video_area left_description">
					<div class="description">
						<h3>Just the Facts</h3>
						<p>Varicose veins are veins that have become enlarged and twisted. They are distinguished from reticular veins (blue veins) and telangiectasias (spider veins), which also involve valvular insufficiency, by their size and location. Varicose veins are common in the superficial veins of the legs, which are subject to high pressure when standing. Besides cosmetic problems, varicose veins are often painful, especially when standing or walking. They often itch, and scratching them can cause ulcers. Serious complications are rare.</p>
					</div>
					<div class="video">
						<iframe src="//player.vimeo.com/video/18894562?title=0&amp;byline=0&amp;portrait=0&amp;color=00AEC5" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
				</div>
				<h2 class="full">Varicose Vein Treatment Options</h2>
				<div class="two-thirds">
					<h3 class="arrow gap_after">Endovenous Laser Ablation</h3>
					<p>EVLA is a procedure that safely and effectively cures venous insufficiency. By treating the disease process, the symptoms of venous disease — including varicose veins — disappear. Unlike painful varicose vein surgery of the past, EVLA vein treatment shuts down veins from the inside by use of laser and ultrasound technology — all with minimal discomfort. Our initial assessment will include an ultrasonic evaluation of your veins. In real time our experts will make an accurate diagnosis, and an appropriate and effective customized treatment plan can be developed for you. EVLA involves entering the vein and treating the diseased vein from within. Using cutting edge ultrasound technology, the diseased vein is identified and entered using real-time ultrasound guidance.</p>
					<p>A small catheter (or IV) is inserted into the vein. This catheter is then advanced within the vein to treat the entire length of diseased vein. At this point, local anesthesia (also called <em>tumescent anesthesia</em>) is injected around the vein. This solution of local anesthesia serves two purposes. First, the vein itself as well as the surrounding tissues are anesthetized and become numb. Secondly, the fluid around the vein helps to protect the surrounding tissues from any collateral injury from the heat of the laser. Once the anesthesia has been injected around the vein, the laser is activated and the vein causing the patient's symptoms is completely ablated. Long-term follow-up of patients treated in this fashion indicate that less than 5% of patients have a recurrence of their disease.</p>
				</div>
				<div class="third">
					<h3 class="arrow gap_after">Recovery</h3>
					<p>Unlike surgical vein stripping where there is significant down time, patients treated with the EVLA procedure and/or with sclerotherapy usually have no down time, and can normally resume normal activities immediately. Exercise is not only permitted following the EVLA procedure, it is encouraged. Immediately after the EVLA procedure, we encourage patients to walk for 20 minutes. We find that patients who use their leg muscles every day after the EVLA procedure have faster healing, less discomfort, and fewer complications. The simplest and most effective form of exercise to optimize leg vein function is walking. We often have our patients abstain from swimming and bathing (showering is okay) for 5 days following the EVLA procedure.</p>
				</div>
				<div class="two-thirds">
					<h3 class="arrow gap_after">Radiofrequency Ablation</h3>
					<p>Radiofrequency ablation (RFA) is a minimally invasive procedure which uses segmental radiofrequency energy to provide an even and uniform heat to contract the collagen in the vein walls, causing them to collapse and seal the vein closed. Blood is then re-routed through healthy veins back towards the heart.</p>
					<p>The procedure involves inserting a catheter into the diseased vein through a small incision below the knee and threading the catheter through the vessel. The catheter delivers energy to the diseased vessel causing it to collapse. Blood flow is then naturally redirected to healthy veins as the recovery process begins. The treated vein becomes scar tissue and is eventually absorbed by the body.</p>
					<p>Based on certain anatomic considerations, your surgeon will select either EVLA or RFA as the energy source to use in closing your abnormal vein.</p>
				</div>
				<div class="third">
					<h3 class="arrow gap_after">Recovery</h3>
					<p>Unlike surgical vein stripping where there is significant down time, patients treated with the RFA procedure and/or with sclerotherapy usually have no down time, and can normally resume normal activities immediately. Exercise is not only permitted following the RFA procedure, it is encouraged. Immediately after the RFA procedure, we encourage patients to walk for 20 minutes. We find that patients who use their leg muscles every day after the RFA procedure have faster healing, less discomfort, and fewer complications. The simplest and most effective form of exercise to optimize leg vein function is walking. We often have our patients abstain from swimming and bathing (showering is okay) for 5 days following the RFA procedure.</p>
				</div>
				<div class="two-thirds">
					<h3 class="arrow gap_after">Sclerotherapy</h3>
					<p>Sclerotherapy involves the injection of a chemical agent into diseased veins. These veins typically are the smaller spider veins or slightly bigger reticular veins and even some larger varicose veins. The agent is injected directly into the vein and causes damage to the lining of the vein, which eventually causes the vein to completely disappear. Depending on the size of the vein, the solution can be agitated causing it to foam prior to injection. Foam sclerotherapy is very effective for some of the larger vessels.</p>
					<p>We use only the latest agents for injection — those that have been clinically proven to be effective. We have carefully chosen agents that are very low-risk and have extremely high efficacy. In addition, we adjust the concentration of the medication based on each individual patient. The procedure is essentially pain-free. We use extremely small needles for the injections. Most patients do not feel the needle enter the vein. The solution itself does not cause any discomfort and the procedure is typically nearly painless. Typically, the veins that are visible represent the tip of the iceberg. There are usually veins slightly deeper and somewhat bigger that are the actual source of the spider veins that are visible. Rather than simply injecting the visible veins, we strive to address the source of the problem. This not only results in much better long-term results; the cosmetic benefit is improved as well. Usually this requires the use of the vein viewer. This allows us to see those veins not visible to the naked eye. These veins tend to be the actual culprit and if they are not treated, the end result will be sub-optimal.</p>
				</div>
				<div class="third">
					<h3 class="arrow gap_after">Recovery</h3>
					<p>After the procedure there are no restrictions and any activities can be resumed.</p>
				</div>
				<h2 class="full">The Results</h2>
				<div class="half before_after">
					<img alt="Before varicose vein treatment" src="/images/varicose_veins_before.png">
					<h3>Before Treatment</h3>
				</div>
				<div class="half before_after">
					<img alt="After varicose vein treatment" src="/images/varicose_veins_after.png">
					<h3>After Treatment</h3>
				</div>
			</div>
		</div>
	</section>
	<section class="gray bottom_layer rollout_content" data-rollout-id="spider_veins">
		<div class="width_limiter">
			<div class="block_container">
				<h2 class="full long_top">Spider Veins<a href="#" class="rollout_close" title="Close Details"></a></h2>
				<div class="full video_area left_description">
					<div class="description">
						<h3>Just the Facts</h3>
						<p>Spider veins are small abnormal veins that are usually red, purple or blue. Sometimes spider veins are only a cosmetic issue, but they are often a sign of a problem in other larger, deeper veins that aren't visible. In this case, the deeper veins must be evaluated by ultrasound. Many people have lived with these symptoms for so long that they don't realize that the symptoms are being caused by their veins. The good news is that treating spider veins, usually with a combination of sclerotherapy and a laser procedure, will help improve these symptoms.</p>
					</div>
					<div class="video">
						<iframe src="//player.vimeo.com/video/18891712?title=0&amp;byline=0&amp;portrait=0&amp;color=00AEC5" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
				</div>
				<h2 class="full">Spider Vein Treatment</h2>
				<div class="two-thirds">
					<h3 class="arrow gap_after">Details</h3>
					<p>If the ultrasound performed during your initial evaluation shows that the deeper veins in your legs are not working correctly, you will need a combination of sclerotherapy and an ablation procedure called endovenous thermal ablation (EVTA) to treat your veins effectively. In this case, these procedures are almost always covered by health insurance. If the ultrasound shows that the deeper veins are working properly, you will only need sclerotherapy. Unfortunately, many insurance companies have stopped paying for sclerotherapy alone; they will only cover sclerotherapy if you also need the EVTA procedure.</p>
					<p>If your examination and ultrasound show that your spider veins are more than cosmetic, and you do need the EVTA procedure, our office will submit this information to your insurance company to get authorization for your treatments. After this, we will call you to schedule you for treatment. If your exam shows that your spider veins are truly cosmetic, your insurance will not pay for treatment in most cases. If this is the case, we will give you an estimate of the cost to treat your spider veins, and will schedule you for treatment at your convenience.</p>
				</div>
				<div class="third">
					<h3 class="arrow gap_after">Recovery</h3>
					<p>After the procedure there are no restrictions and any activities can be resumed.</p>
				</div>
				<h2 class="full">The Results</h2>
				<div class="half before_after">
					<img alt="Before spider vein treatment" src="/images/spider_veins_before.png">
					<h3>Before Treatment</h3>
				</div>
				<div class="half before_after">
					<img alt="After spider vein treatment" src="/images/spider_veins_after.png">
					<h3>After Treatment</h3>
				</div>
			</div>
		</div>
	</section>
	<?php echo_section('footer'); ?>
</body>
<?php close_document();
