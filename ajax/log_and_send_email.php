<?php
error_reporting(0);
require_once('../config.php');
require_once('../db.php');
$db = new DatabaseController('mysqli');
require_once('../plugins/swiftmailer413/swift_required.php');

// Check for spam (we randomly call the field "jdarc" to fool bots)
$captcha = (int)$_POST['jdarc'];
if ($captcha != $_POST['st'] - 1) {
	$result = array(
		'status' => 'error',
		'errors' => array(
			"You didn't fill out all fields correctly."
		)
	);
	echo(json_encode($result));
	die();
}

$message = new EmailMessage(array(
	'email_type' => $_POST['email_type'],
	'name' => $_POST['name'],
	'phone' => $_POST['phone'],
	'email' => $_POST['email'],
	'message' => $_POST['message'],
	'reason' => $_POST['reason'],
	'preferred_contact' => $_POST['preferred_contact']
));
EmailHandler::log_to_db($message, $db);
EmailHandler::send($message);
if (count(EmailHandler::$errors)) {
	$result['status'] = 'error';
	$result['errors'] = EmailHandler::$errors;
} else {
	$result['status'] = 'ok';
}
echo(json_encode($result));
class EmailHandler {
	public static $errors = array();
	public static function log_to_db($message, $db) {
		$qstr = "INSERT INTO client_emails (email_type, name, phone, email, message, reason, preferred_contact) VALUES (?, ?, ?, ?, ?, ?, ?)";
		if ($q = $db->db()->prepare($qstr)) {
			$bound = $q->bind_param('sssssss',
				$message->get_email_type(),
				$message->get_name(),
				$message->get_phone(),
				$message->get_email(),
				$message->get_message(),
				$message->get_reason(),
				$message->get_preferred_contact()
			);
		} else {
			$this->errors[] = "Could not prepare database insert. Error #{$db->db()->errno}: {$db->db()->error}";
		}
		if ($bound) {
			if ($q->execute()) {
				$q->close();
			} else {
				$this->errors[] = "Could not execute database insert. Error #{$db->db()->errno}: {$db->db()->error}";
			}
		} else {
			$this->errors[] = "Could not bind parameter(s). Error #{$db->db()->errno}: {$db->db()->error}";
		}
	}
	public static function send($message) {
		global $FYL_GLOBALS;
		$swift_message = Swift_Message::newInstance()
			->setSubject($message->get_subject())
			->setFrom($FYL_GLOBALS['mail_from'])
			->setTo($FYL_GLOBALS['mail_recipients'])
			->setBody(EmailMessageFormatter::format_message($message, 'prepend_contact_info', 'plain_text'))
		;
		$transport = Swift_SmtpTransport::newInstance($FYL_GLOBALS['mail_server'], $FYL_GLOBALS['mail_port'], $FYL_GLOBALS['mail_security'])
			->setUsername($FYL_GLOBALS['mail_username'])
			->setPassword($FYL_GLOBALS['mail_password'])
		;
		$mailer = Swift_Mailer::newInstance($transport);
		$result = $mailer->send($swift_message);
		return $result;
	}
}
class EmailMessage {
	private $email_type;
	private $subject;
	private $name;
	private $phone;
	private $email;
	private $message;
	private $reason;
	private $preferred_contact;
	private function init_blank() {
		$this->set_email_type('message');
		$this->set_subject('');
		$this->set_name('');
		$this->set_phone('');
		$this->set_email('');
		$this->set_message('');
		$this->set_reason('');
		$this->set_preferred_contact('');
	}
	function __construct() {
		if (func_num_args() > 0) {
			$args = func_get_arg(0);
			if (!is_array($args)) {
				$this->init_blank;
			} else {
				if (array_key_exists('email_type', $args)) {$this->set_email_type($args['email_type']);} else {$this->set_email_type('');}
				if (array_key_exists('subject', $args)) {
					$this->set_subject($args['subject']);
				} else {
					if ($this->get_email_type() == 'message') {
						$this->set_subject('Message from Website');
					} else if ($this->get_email_type() == 'appointment') {
						$this->set_subject('Appointment Request from Website');
					} else {
						$this->set_subject('');
					}
				}
				if (array_key_exists('name', $args)) {$this->set_name($args['name']);} else {$this->set_name('');}
				if (array_key_exists('phone', $args)) {$this->set_phone($args['phone']);} else {$this->set_phone('');}
				if (array_key_exists('email', $args)) {$this->set_email($args['email']);} else {$this->set_email('');}
				if (array_key_exists('message', $args)) {$this->set_message($args['message']);} else {$this->set_message('');}
				if (array_key_exists('reason', $args)) {$this->set_reason($args['reason']);} else {$this->set_reason('');}
				if (array_key_exists('preferred_contact', $args)) {$this->set_preferred_contact($args['preferred_contact']);} else {$this->set_preferred_contact('');}
			}
		} else {
			$this->init_blank();
		}
	}
	private function is_email_type_allowed($type) {
		return in_array($type, array('message', 'appointment'));
	}
	function set_email_type($value) {
		if($this->is_email_type_allowed($value)) {
			$this->email_type = $value;
		} else {
			trigger_error("Value \"{$this->email_type}\" is not allowed for EmailMessage::\$email_type. Type assumed is \"message\".", E_USER_WARNING);
			$this->email_type = 'message';
		}
	}
	function get_email_type() {return $this->email_type;}
	function set_subject($value) {
		if ($value == "" || $value == false) {
			trigger_error("Blank message subjects not allowed for EmailMessage. Subject set to \"No Subject\".", E_USER_WARNING);
			$this->subject = 'No Subject';
		} else {
			$this->subject = $value;
		}
	}
	function get_subject() {return $this->subject;}
	function set_name($value) {$this->name = $value;}
	function get_name() {return $this->name;}
	function set_phone($value) {$this->phone = $value;}
	function get_phone() {return $this->phone;}
	function set_email($value) {$this->email = $value;}
	function get_email() {return $this->email;}
	function set_message($value) {$this->message = $value;}
	function get_message() {return $this->message;}
	function set_reason($value) {$this->reason = $value;}
	function get_reason() {return $this->reason;}
	function set_preferred_contact($value) {$this->preferred_contact = $value;}
	function get_preferred_contact() {return $this->preferred_contact;}
}
class EmailMessageFormatter {
	public static function format_message($message, $style) {
		$output = "";
		switch($style) {
			case 'prepend_contact_info': {
				switch($message->get_email_type()) {
					case 'message': {
						$output = "You have received a new message from the Vein Care Institute website. The details are below.\n\nName: " . $message->get_name() . "\nEmail Address: " . $message->get_email() . "\nPhone: " . $message->get_phone() . "\n\nMessage: " . $message->get_message();
						break;
					}
					case 'appointment': {
						$output = "You have received an appointment request from the Vein Care Institute website. The details are below.\n\nName: " . $message->get_name() . "\nEmail Address: " . $message->get_email() . "\nPhone: " . $message->get_phone() . "\nPreferred Contact: " . $message->get_preferred_contact() . "\nReason for Appointment: " . $message->get_reason();
						break;
					}
				}
				break;
			}
			default: {
				trigger_error("Unknown style specified for EmailMessageFormatter::format_message(); no styling applied", E_USER_WARNING);
				$output = $message->get_message();
				break;
			}
		}
		return $output;
	}
}
die();
switch ($_POST['email_type']) {
	case 'message': { // Using the form in the footer.
		$qstr = "INSERT INTO client_emails (email_type, name, phone, email, message) VALUES (?, ?, ?, ?, ?)";
		if ($q = $db->db()->prepare($qstr)) {
			$bound = $q->bind_param('sssss',
				$_POST['email_type'],
				$_POST['name'],
				$_POST['phone'],
				$_POST['email'],
				$_POST['message']
			);
		} else {
			$errors[] = "Could not prepare database insert. Error #{$db->db()->errno}: {$db->db()->error}";
		}
		break;
	}
	case 'appointment': { // Using the form toward the top of the page.
		$qstr = "INSERT INTO client_emails (email_type, name, phone, email, reason, preferred_contact) VALUES (?, ?, ?, ?, ?, ?)";
		if ($q = $db->db()->prepare($qstr)) {
			$bound = $q->bind_param('ssssss',
				$_POST['email_type'],
				$_POST['name'],
				$_POST['phone'],
				$_POST['email'],
				$_POST['reason'],
				$_POST['preferred_contact']
			);
		} else {
			$errors[] = "Could not prepare database insert. Error #{$db->db()->errno}: {$db->db()->error}";
		}
		break;
	}
}
if ($bound) {
	if ($q->execute()) {
		$q->close();
		//Send email

	} else {
		$errors[] = "Could not execute database insert. Error #{$db->db()->errno}: {$db->db()->error}";
	}
} else {
	$errors[] = "Could not bind parameter(s). Error #{$db->db()->errno}: {$db->db()->error}";
}
if (count($errors)) {
	$result['status'] = 'error';
	$result['errors'] = $errors;
} else {
	$result['status'] = 'ok';
}
echo(json_encode($result));
