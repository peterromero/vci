<?php
error_reporting(0);
$results = array();
$results['status'] = 'ok';
require_once('../config.php');
require_once('../db.php');
$db = new DatabaseController('mysqli');
$d = new RecursiveDirectoryIterator('../');
$i = new RecursiveIteratorIterator($d);
$php_files = new RegexIterator($i, '/^.+\.php$/i', RecursiveRegexIterator::GET_MATCH);
// Put files to search in with paths relative to the /ajax directory, using backslashes locally but forward slashes remotely.
$files_to_search = array(
	'..' . $FYL_GLOBALS['directory_separator'] . 'index.php',
	'..' . $FYL_GLOBALS['directory_separator'] . 'about_vci' . $FYL_GLOBALS['directory_separator'] . 'index.php',
	'..' . $FYL_GLOBALS['directory_separator'] . 'diagnosis_and_treatment' . $FYL_GLOBALS['directory_separator'] . 'index.php',
	'..' . $FYL_GLOBALS['directory_separator'] . 'insurance' . $FYL_GLOBALS['directory_separator'] . 'index.php'
);
$db->db()->query('delete from search_index');
foreach($php_files as $php_filename) {
	if (in_array($php_filename[0], $files_to_search)) {
		ob_start();
		require($php_filename[0]);
		$contents = ob_get_contents();
		ob_end_clean();
		$q_str = "insert into search_index (`page_title`, `page_url`, `terms`, `fulltext`) values (?, ?, ?, ?)";
		preg_match('/(?<=\<title\>).*(?=\<\/title\>)/', $contents, $title_tags);
		$terms = get_term_list($contents);
		$fulltext = get_sanitized_string($contents);
		$filename = str_replace(array('\\', '../', 'index.php'), array('/', '/', ''), $php_filename[0]);
		if ($q = $db->db()->prepare($q_str)) {
			$bound = $q->bind_param('ssss',
				$title_tags[0],
				$filename,
				$terms,
				$fulltext
			);
		} else {
			$results['status'] = 'error';
			$results['messages'][] = "Could not prepare database insert. Error #{$db->db()->errno}: {$db->db()->error}";
		}
		if ($bound) {
			if ($q->execute()) {
				$results['messages'][] = "Successfully indexed file {$filename} (title: {$title_tags[0]})";
				$q->close();
			} else {
				$results['status'] = 'error';
				$results['messages'][] = "Could not execute database insert. Error #{$db->db()->errno}: {$db->db()->error}";
			}
		} else {
			$results['status'] = 'error';
			$results['messages'][] = "Could not bind parameter(s). Error #{$db->db()->errno}: {$db->db()->error}";
		}
	}
}
echo(json_encode($results));
function get_sanitized_string($input) {
	$input = str_replace('<br>', ' ', $input);
	$input = html_entity_decode(strip_tags($input));
	$input = preg_replace('/\n|\r|\t|\s/', ' ', $input);
	$input = preg_replace(array('/\s+/', '/^ /', '/ $/'), array(' ', '', ''), $input);
	return $input;
}
function get_term_list($input) {
	$sanitized_string = get_sanitized_string($input);
	$searchable_string = preg_replace('/[^\w ]/', '', $sanitized_string);
	$searchable_string = strtolower($searchable_string);
	$terms = array();
	$token = strtok($searchable_string, ' ');
	while ($token !== false) {
		if (strlen($token) > 2) {
			$terms[] = $token;
		}
		$token = strtok(' ');
	}
	$terms = array_unique($terms);
	sort($terms);
	$output = implode(' ', $terms);
	return $output;
}