/****************************************
CUSTOM CAROUSEL FOR VCI HOME PAGE

Basic HTML setup consists of a wrapper div:

<div class="carousel">
</div>

containing one or more slides, either <div>s or <a>s.

	<div> ... </div>
	<a> ... </a>

Images should go within a slide, not just within the wrapper.

	<div><img src="..."></div>
	<a><img src="..."></a>

All images should have the same width and height (960 × 500).

Carousel should have a minimum of three slides to work.

*****************************************/
var vci_carousels = [];
$(function() {
	$('.carousel').addClass('loading');
	$('.carousel img').each(function() {
		// We append a random string to all image sources to force the load() event to fire, even in IE.
		$(this).attr('src', $(this).attr('src') + '?x=' + new Date().getTime());
	});
	$('body').append('<div class="carousel_slide_width_slug"></div>');
	// If this ever stops working, it could be because the jQuery load function has been deprecated.
	$('.carousel img').first().load(function() {
		carousel($('.carousel'), {auto_advance: true});
	});
});
$(window).resize(function() {
	for (i in vci_carousels) {
		_position_lineup({
			carousel_wrapper: vci_carousels[i].wrapper,
			how: 'immediate'
		});
		_add_arrows_if_necessary_and_position(vci_carousels[i].wrapper);
	}
});
$.fn.next_slide = function() {
	$(this).addClass('disallow_user_scrolling');
	var new_slide_index = range_mod({
		value: $(this).data('options').current_slide_index + 1,
		min: 0,
		max: $(this).data('slides').length - 1
	});
	$(this).data('options').current_slide_index = new_slide_index;
	_define_lineup($(this));
	_position_lineup({
		carousel_wrapper: $(this),
		how: 'animate',
		direction: 'to_left'
	});
}
$.fn.prev_slide = function() {
	var new_slide_index = range_mod({
		value: $(this).data('options').current_slide_index - 1,
		min: 0,
		max: $(this).data('slides').length - 1
	});
	$(this).data('options').current_slide_index = new_slide_index;
	_define_lineup($(this));
	_position_lineup({
		carousel_wrapper: $(this),
		how: 'animate',
		direction: 'to_right'
	});
}
function carousel($wrappers, options) {
	var default_args = {
		current_slide_index: 0,
		slide_spacing: 20,
		transition_duration: 750,
		transition_easing: 'easeOutExpo',
		auto_advance: true,
		slide_duration: 10000
	}
	if (typeof(options) != 'object') options = {};
	for (var i in default_args) {
		if (typeof options[i] == 'undefined') {
			options[i] = default_args[i];
		}
	}
	$wrappers.each(function() {
		vci_carousels.push({wrapper: $(this)});
		var $wrapper = $(this);
		$wrapper.data('options', {});
		for (var i in options) { //We have to copy options over one at a time or else we'll end up with a reference to the same object in all carousels on the page, coercing them to be on the same slide and other nonsense. Not good.
			$wrapper.data('options')[i] = options[i];
		}
		$wrapper.data('slides', $wrapper.children());
		$wrapper.data('slides').each(function() {
			$(this).addClass('slide');
			$(this).css('top', ($wrapper.height() - $(this).height()) / 2);
			// Prevent clicks on slides other than the center one.
			$(this).click(function(e) {
				if ($(this).is('a') && !$(this).hasClass('center')) {
					e.preventDefault();
				}
			});
		});
		_define_lineup($wrapper);
		_position_lineup({
			carousel_wrapper: $wrapper,
			how: 'immediate'
		});
		_add_arrows_if_necessary_and_position($wrapper);
		_enable_touch_gestures($wrapper);
		$wrapper.children('.slide').css('visibility', 'visible').css('opacity', 1);
		setTimeout(function() {$wrapper.removeClass('loading');}, 1000);
		if ($wrapper.data('options').auto_advance) {
			$wrapper.data('advance', setInterval(function() {$wrapper.next_slide();}, $wrapper.data('options').slide_duration));
		}
	});
}
function _enable_touch_gestures($carousel_wrapper) {
	$carousel_wrapper.find('.slide').hammer({prevent_default: true}).on("swiperight", function(event) {
		if (!$carousel_wrapper.hasClass('disallow_user_scrolling')) {
			_cancel_auto_advance_if_set($carousel_wrapper);
			$carousel_wrapper.prev_slide();
		}
	});
	$carousel_wrapper.find('.slide').hammer({prevent_default: true}).on("swipeleft", function(event) {
		if (!$carousel_wrapper.hasClass('disallow_user_scrolling')) {
			_cancel_auto_advance_if_set($carousel_wrapper);
			$carousel_wrapper.next_slide();
		}
	});
}
function _cancel_auto_advance_if_set($carousel_wrapper) {
	if ($carousel_wrapper.data('advance')) {
		clearInterval($carousel_wrapper.data('advance'));
	}
}
function _add_arrows_if_necessary_and_position($carousel_wrapper) {
	var arrow_size = {width: 40, height: 75};
	if ($carousel_wrapper.find('.carousel_arrow').length == 0) {
		$('<a href="#" class="left carousel_arrow">&nbsp;</a>').appendTo($carousel_wrapper);
		$('<a href="#" class="right carousel_arrow">&nbsp;</a>').appendTo($carousel_wrapper);
		$carousel_wrapper.data('arrows', {});
		$carousel_wrapper.data('arrows').left = $carousel_wrapper.find('.left.carousel_arrow');
		$carousel_wrapper.data('arrows').right = $carousel_wrapper.find('.right.carousel_arrow');
	}
	var top = ($carousel_wrapper.data('lineup').center_slide.height() - arrow_size.height) / 2;
	var left = -$carousel_wrapper.data('lineup').center_slide.width() / 2 - arrow_size.width - 20 - $carousel_wrapper.data('options').slide_spacing;
	var right = $carousel_wrapper.data('lineup').center_slide.width() / 2 + 20 + $carousel_wrapper.data('options').slide_spacing;
	$carousel_wrapper.data('arrows').left
		.css('top', top)
		.css('margin-left', left)
		.click(function(e) {
			e.preventDefault();
			if (!$carousel_wrapper.hasClass('disallow_user_scrolling')) {
				_cancel_auto_advance_if_set($carousel_wrapper);
				$carousel_wrapper.prev_slide();
			}
		})
	;
	$carousel_wrapper.data('arrows').right
		.css('top', top)
		.css('margin-left', right)
		.click(function(e) {
			e.preventDefault();
			if (!$carousel_wrapper.hasClass('disallow_user_scrolling')) {
				_cancel_auto_advance_if_set($carousel_wrapper);
				$carousel_wrapper.next_slide();
			}
		})
	;
}
function _define_lineup($carousel_wrapper) {
	$carousel_wrapper.data('lineup', {});
	$carousel_wrapper.data('lineup').center_slide = $carousel_wrapper.data('slides').eq($carousel_wrapper.data('options').current_slide_index);
	var right_slide_index = range_mod({
		value: $carousel_wrapper.data('options').current_slide_index + 1,
		min: 0,
		max: $carousel_wrapper.data('slides').length - 1
	});
	var left_slide_index = range_mod({
		value: $carousel_wrapper.data('options').current_slide_index - 1,
		min: 0,
		max: $carousel_wrapper.data('slides').length - 1
	});
	var right_right_slide_index = range_mod({
		value: $carousel_wrapper.data('options').current_slide_index + 2,
		min: 0,
		max: $carousel_wrapper.data('slides').length - 1
	});
	var left_left_slide_index = range_mod({
		value: $carousel_wrapper.data('options').current_slide_index - 2,
		min: 0,
		max: $carousel_wrapper.data('slides').length - 1
	});
	$carousel_wrapper.data('lineup').right_slide = $carousel_wrapper.data('slides').eq(right_slide_index);
	$carousel_wrapper.data('lineup').left_slide = $carousel_wrapper.data('slides').eq(left_slide_index);
	$carousel_wrapper.data('lineup').right_right_slide = $carousel_wrapper.data('slides').eq(right_right_slide_index);
	$carousel_wrapper.data('lineup').left_left_slide = $carousel_wrapper.data('slides').eq(left_left_slide_index);
}
function _position_lineup(args) {
	__resize_carousel(args.carousel_wrapper);
	args.carousel_wrapper.data('slides').css('display', 'none').removeClass('center');
	args.carousel_wrapper.data('lineup').center_slide.css('display', 'block').addClass('center');
	args.carousel_wrapper.data('lineup').right_slide.css('display', 'block');
	args.carousel_wrapper.data('lineup').left_slide.css('display', 'block');
	if (args.direction == 'to_left') {
		args.carousel_wrapper.data('lineup').left_left_slide.css('display', 'block');
		args.carousel_wrapper.data('lineup').right_slide.css('margin-left', args.carousel_wrapper.data('lineup').left_slide.width() / 2 + 2 * args.carousel_wrapper.data('options').slide_spacing + args.carousel_wrapper.data('lineup').center_slide.width());
	};
	if (args.direction == 'to_right') {
		args.carousel_wrapper.data('lineup').right_right_slide.css('display', 'block');
		args.carousel_wrapper.data('lineup').left_slide.css('margin-left', -args.carousel_wrapper.data('lineup').right_slide.width() / 2 - 2 * args.carousel_wrapper.data('options').slide_spacing - args.carousel_wrapper.data('lineup').center_slide.width() - args.carousel_wrapper.data('lineup').left_slide.width());
	};
	switch(args.how) {
		case 'immediate': {
			args.carousel_wrapper.data('lineup').center_slide.css('margin-left', -args.carousel_wrapper.data('lineup').center_slide.width() / 2);
			args.carousel_wrapper.data('lineup').right_slide.css('margin-left', args.carousel_wrapper.data('lineup').center_slide.width() / 2 + args.carousel_wrapper.data('options').slide_spacing);
			args.carousel_wrapper.data('lineup').left_slide.css('margin-left', -args.carousel_wrapper.data('lineup').center_slide.width() / 2 - args.carousel_wrapper.data('options').slide_spacing - args.carousel_wrapper.data('lineup').left_slide.width());
			args.carousel_wrapper.removeClass('disallow_user_scrolling');
			break;
		}
		case 'animate': {
			args.carousel_wrapper.data('lineup').center_slide.animate(
				{'margin-left': -args.carousel_wrapper.data('lineup').center_slide.width() / 2},
				args.carousel_wrapper.data('options').transition_duration,
				args.carousel_wrapper.data('options').transition_easing,
				function() {
					args.carousel_wrapper.removeClass('disallow_user_scrolling');
				}
			);
			args.carousel_wrapper.data('lineup').right_slide.animate(
				{'margin-left': args.carousel_wrapper.data('lineup').center_slide.width() / 2 + args.carousel_wrapper.data('options').slide_spacing},
				args.carousel_wrapper.data('options').transition_duration,
				args.carousel_wrapper.data('options').transition_easing
			);
			args.carousel_wrapper.data('lineup').left_slide.animate(
				{'margin-left': -args.carousel_wrapper.data('lineup').center_slide.width() / 2 - args.carousel_wrapper.data('options').slide_spacing - args.carousel_wrapper.data('lineup').left_slide.width()},
				args.carousel_wrapper.data('options').transition_duration,
				args.carousel_wrapper.data('options').transition_easing
			);
			if (args.direction == 'to_left') {
				args.carousel_wrapper.data('lineup').left_left_slide.animate(
					{'margin-left': -args.carousel_wrapper.data('lineup').center_slide.width() / 2 - 2 * args.carousel_wrapper.data('options').slide_spacing - args.carousel_wrapper.data('lineup').left_slide.width() - args.carousel_wrapper.data('lineup').left_left_slide.width()},
					args.carousel_wrapper.data('options').transition_duration,
					args.carousel_wrapper.data('options').transition_easing
				);
			}
			if (args.direction == 'to_right') {
				args.carousel_wrapper.data('lineup').right_right_slide.animate(
					{'margin-left': args.carousel_wrapper.data('lineup').center_slide.width() / 2 + 2 * args.carousel_wrapper.data('options').slide_spacing + args.carousel_wrapper.data('lineup').right_slide.width()},
					args.carousel_wrapper.data('options').transition_duration,
					args.carousel_wrapper.data('options').transition_easing
				);
			}
			break;
		}
	}
}
function __resize_carousel($carousel_wrapper) {
	var width = $('.carousel_slide_width_slug').width();
	$carousel_wrapper.data('slides').each(function() {
		$(this).width(width);
		$(this).children('img').width(width);
	});
	// We measure the height of the center image, which will be different depending on what slide we're on
	// (even though all images should be the same dimensions).
	// We do that because at any given moment some images are within slides that are display: none, which
	// will cause them to report zero height. Thus we can't rely on the height of one image only.
	var height = $carousel_wrapper.data('lineup').center_slide.find('img').height();
	$carousel_wrapper.height(height);
	$carousel_wrapper.data('slides').each(function() {
		$(this).height(height);
	});
	vci_app.vertical_layout();
}
function range_mod(args) {
	var result;
	if (args.value >= args.min && args.value <= args.max) {
		result = args.value;
	} else {
		var range_width = args.max - args.min + 1;
		var shifted_value = args.value - args.min;
		while (shifted_value < 0) {
			shifted_value += range_width;
		}
		var shifted_mod = shifted_value % range_width;
		var mod = shifted_mod + args.min;
		result = mod;
	}
	return result;
}