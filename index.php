<?php
$absolute_path_to_root = $_SERVER['DOCUMENT_ROOT'] . '/';
$relative_path_to_root = './';
$nav_highlighted_page = 'home';
$unique_page_id = 'home';
require_once("{$absolute_path_to_root}templating.php");
open_document();
echo_head_section(array(
  'title' => ':Non-Surgical Varicose &amp; Spider Vein Removal',
  'description' => 'Based in Denver, the Vein Care Institute specializes in diagnosis and treatment of varicose veins, spider veins, all types of vein disorders with laser therapy.',
  'keywords' => 'vein care, varicose veins, endovenous laser therapy, spider veins, vascular surgery, sclerotherapy',
  'enable_carousel' => TRUE
));
?>
  <body class="page_<?php echo($unique_page_id); ?>">
    <?php echo_section('masthead'); ?>
    <?php echo_section('nav_menu'); ?>
    <?php echo_section('resources_rollout', array('location' => 'top')); ?>
    <?php echo_section('forms_rollout', array('location' => 'top')); ?>
    <?php echo_section('carousel'); ?>
    <?php echo_section('nav_strip'); ?>
    <?php echo_section('resources_rollout', array('location' => 'side')); ?>
    <?php echo_section('forms_rollout', array('location' => 'side')); ?>
    <section class="gray bottom_layer first">
      <div class="width_limiter">
        <div class="block_container">
          <h2 class="full">Free Your Legs</h2>
          <div class="half long_bottom">
            <p>At The Vein Care Institute, we specialize in helping you to
              reclaim your life by freeing you from the vein disorders that have
              taken over your legs — and your life. Located in Denver Colorado,
              our industry leading doctors specialize in the treatment and
              removal of varicose veins, spider veins and venous insufficiency,
              while achieving superior physical and aesthetic results. That
              means your legs will not only feel better, but they'll also LOOK
              better, so you can wear whatever you want to wear without the
              self-consciousness that often accompanies unsightly vein problems.<br><br><a
                href="/about_vci"
                class="big_link">More about the Vein Care Institute</a></p>
          </div>
          <div class="half long_bottom">
            <p>At our state-of-art facility, you'll find the most up-to-date
              technology and equipment for diagnosis and treatment of all types
              of vein disorders. We also have the most highly-trained and caring
              staff, so you can feel confident that you're working with experts
              whose only goal is to bring you relief. We also understand that
              each patient's experience with vein disorders can be as unique as
              each individual, which is why we customize your treatment
              according to your specific symptoms and health history.<br><br><a
                href="/diagnosis_and_treatment"
                class="big_link">Learn About Our Treatment Options</a></p>
          </div>
        </div>
      </div>
    </section>
    <section class="white top_layer">
      <div class="width_limiter">
        <div class="block_container certification">
          <div class="sixth">
            <p class="certification-badge">
              <a href="https://www.absurgery.org/default.jsp?publiccertprocess"
                 target="_blank"><img alt="Certifed by the American Board of Surgery"
                                      title="Certifed by the American Board of Surgery"
                                      width="100px"
                                      src="/images/abos-badge.png"></a></p>
          </div>
          <div class="five-sixths">
            <p class="certification-notice">Both of our surgeons, William Schuh,
              MD and Brian Ridge, MD, are certified by the
              <a href="https://www.absurgery.org/default.jsp?publiccertprocess"
                 target="_blank">American Board of Surgery</a>. When looking for
              a physician, always be sure to go to a board-certified vascular
              surgeon!
            </p>
          </div>
        </div>
      </div>
    </section>
    <section class="gray bottom_layer">
      <div class="width_limiter">
        <div class="block_container">
          <h2 class="full long_top">Benefits of Treatment</h2>
          <div class="third">
            <h3 class="arrow gap_after">Pain Relief</h3>
            <p class="long_bottom">If you've been living with varicose veins,
              spider veins, or the often undiagnosed and potentially severe case
              of venous insufficiency, you've also likely been living with a
              constant dull aching, heaviness or cramping in your legs, as well
              as itching and swelling that often accompanies vein disorders. Let
              us treat your condition and eliminate that pain and discomfort…
              because you have so many other things in life to do!
            </p>
          </div>
          <div class="third">
            <h3 class="arrow gap_after">Return of Self Confidence</h3>
            <p class="long_bottom">One of the biggest issues associated with
              vein disorders is their unsightly appearance, which likely has
              caused a certain level of embarrassment. But with treatment at the
              Vein Care Institute, your legs will not only feel better, they'll
              LOOK better — no more worries about wearing shorts or skirts, no
              more feeling self-conscious in a swimming suit, no more sitting on
              the sidelines of your life!
            </p>
          </div>
          <div class="third">
            <h3 class="arrow gap_after">Improved Overall Health</h3>
            <p class="long_bottom">Once you stop feeling the pain of vein
              disorders, you'll begin to get back into life. Go for a walk, ride
              your bike, play tennis, take a swim — get active again! When you
              can do whatever you want to do without having to worry about the
              discomfort in your legs, you'll be amazed at how your health, as
              well as your outlook on life, will start to improve!
            </p>
          </div>
        </div>
      </div>
      <div class="width_limiter">
        <div class="block_container">
          <div class="full video_area left_description">
            <div class="description">
              <h3>See the difference: Richard Gangl</h3>
              <p>"I thought my problems were due to age; I had no idea they were
                part of a vein disease. I knew my varicose veins were unsightly,
                but I never dreamt they would affect my performance, or more
                importantly my health. After my treatment at Vein Care Institute
                I was able to return to riding my bike almost immediately. I had
                more energy, my breathing was better, and I noticed a
                significant performance improvement in my ride times."
              </p>
            </div>
            <div class="video">
              <iframe src="//player.vimeo.com/video/18886637?title=0&amp;byline=0&amp;portrait=0&amp;color=00AEC5"
                      webkitallowfullscreen
                      mozallowfullscreen
                      allowfullscreen></iframe>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="white top_layer">
      <div class="width_limiter">
        <div class="block_container">
          <h2 class="full long_top">You're Not Alone!</h2>
          <p class="two-thirds">Who gets varicose veins? The truth is, 41% of
            women and 24% of men aged 40–49 years, and 72% of women and 43% of
            men aged 60–69 years have varicose veins.
          </p>
          <div class="chart ring clear quarter_half">
            <canvas data-value="41" data-label="of Women*Aged 40–49"></canvas>
          </div>
          <div class="chart ring quarter_half">
            <canvas data-value="24" data-label="of Men*Aged 40–49"></canvas>
          </div>
          <div class="chart ring quarter_half">
            <canvas data-value="72" data-label="of Women*Aged 60–69"></canvas>
          </div>
          <div class="chart ring quarter_half">
            <canvas data-value="43" data-label="of Men*Aged 60–69"></canvas>
          </div>
        </div>
      </div>
    </section>
    <?php echo_section('footer'); ?>
  </body>
<?php close_document();