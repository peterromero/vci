<?php
$paths['home'] = '/';
$paths['about'] = '/about_vci';
$paths['diagnosis'] = '/diagnosis_and_treatment';
$paths['insurance'] = '/insurance';
$paths['faq'] = '/faq';
function open_document($classes = array()) {
  // Optionally pass this function an array of classes to add to the body.
  // Options are:
  // dev: turns on dev mode (all other classes depend on this one being active to work)
  // show_blocks: shows block boundaries
  if (count($classes)) {
    $class_string = ' class="' . implode(' ', $classes) . '"';
  }
  else {
    $class_string = '';
  }
  echo <<<HTML
<!DOCTYPE html>
<html$class_string lang="en" dir="ltr">
HTML;
}

function close_document() {
  echo <<<HTML
<div class="dev_toolbar"></div>
</html>
HTML;
}

function echo_head_section($args = array()) {
  global $relative_path_to_root;
  if (array_key_exists('title', $args)) {
    // You can automatically prefix or suffix the title with "Vein Care Institute" by prefixing or suffixing it with a colon (:).
    // For example, ":About" expands to "Vein Care Institute | About",
    // and "About:" expands to "About | Vein Care Institute".
    $title = preg_replace(array("/^:/", "/:$/"), array(
      "Vein Care Institute | ",
      " | Vein Care Institute"
    ), $args['title']);
  }
  else {
    $title = 'Vein Care Institute';
  }
  if (array_key_exists('description', $args)) {
    $description = $args['description'];
  }
  else {
    $description = 'Based in Denver, the Vein Care Institute specializes in diagnosis and treatment of varicose veins, spider veins, all types of vein disorders with laser therapy.';
  }
  if (array_key_exists('keywords', $args)) {
    $keywords = $args['keywords'];
  }
  else {
    $keywords = '';
  }
  $carousel_script = '';
  $carousel_css = '';
  if (array_key_exists('enable_carousel', $args)) {
    if ($args['enable_carousel'] == TRUE) {
      $carousel_css = "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$relative_path_to_root}plugins/carousel/carousel.css\">";
      $carousel_script = "<script type=\"text/javascript\" src=\"{$relative_path_to_root}plugins/carousel/carousel.js\"></script>";
    }
  }
  $admin_script = '';
  if (array_key_exists('is_admin_page', $args)) {
    if ($args['is_admin_page'] == TRUE) {
      $admin_script = "<script type=\"text/javascript\" src=\"{$relative_path_to_root}js/admin.js\"></script>";
    }
  }
  $html5shiv = (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 8.0') !== FALSE) ? '<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>' : '';
  $ie8styles = (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 8.0') !== FALSE) ? "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$relative_path_to_root}css/ie8.css\">" : '';
  $output = <<<HTML
<head>
	$html5shiv
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>$title</title>
	<link rel="apple-touch-icon" href="{$relative_path_to_root}images/touch-icons/57.png">
	<link rel="apple-touch-icon" sizes="72x72" href="{$relative_path_to_root}images/touch-icons/72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="{$relative_path_to_root}images/touch-icons/114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="{$relative_path_to_root}images/touch-icons/144.png">
	<link rel="shortcut icon" href="{$relative_path_to_root}favicon.ico" type="image/x-icon">
	<meta name="description" content="$description">
	<meta name="keywords" content="$keywords">
	<meta name="apple-mobile-web-app-capable" content="no">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
	<meta name="viewport" content="minimum-scale=1.0, maximum-scale=1.0, width=device-width, user-scalable=no">
	$carousel_css
	<link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{$relative_path_to_root}plugins/jquery.selectBoxIt.js-master/src/stylesheets/jquery.selectBoxIt.css">
	<link rel="stylesheet" type="text/css" href="{$relative_path_to_root}css/assembler.css.php">$ie8styles
	<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||array()).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-57021960-7', 'auto');ga('send', 'pageview');</script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://code.jquery.com/jquery-migrate-1.1.0.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	$carousel_script
	<script type="text/javascript" src="{$relative_path_to_root}plugins/jquery.selectBoxIt.js-master/src/javascripts/jquery.selectBoxIt.min.js"></script>
	<script type="text/javascript" src="{$relative_path_to_root}plugins/hammer.js/hammer.min.js"></script>
	<script type="text/javascript" src="{$relative_path_to_root}plugins/hammer.js/jquery.hammer.min.js"></script>
	<script type="text/javascript" src="{$relative_path_to_root}plugins/jquery-placeholder-master/jquery.placeholder.js"></script>
	<script type="text/javascript" src="{$relative_path_to_root}js/anim.js"></script>
	<script type="text/javascript" src="{$relative_path_to_root}js/main.js"></script>
	$admin_script
</head>
HTML;
  echo($output);
}

function get_section($which, $options = array()) {
  global $relative_path_to_root;
  $output = '';
  switch ($which) {
    case 'masthead':
    {
      $output = <<<HTML
<h1>Vein Care Institute: The Leader in Non-Surgical Varicose and Spider Vein Removal</h1>
<section class="masthead wide">
	<div class="width_limiter">
		<div class="block_container">
			<a class="logo" href="/"></a>
			<p class="contact_slug">Phone: 303 798 3467<br>
			Fax: 303 377 2894<br>
			Email: <a href="mailto:info@freeyourlegs.com">info@freeyourlegs.com</a>
			</p>
		</div>
	</div>
</section>
<section class="masthead narrow">
	<div class="width_limiter">
		<div class="block_container">
			<a class="logo" href="/"></a>
			<a href="#" class="hamburger_toggle"></a>
		</div>
	</div>
</section>
HTML;
      break;
    }
    case 'carousel':
    {
      $output = <<<HTML
<div class="carousel">
	<a href="/about_vci">
		<div class="slide_overlay">
			<h2>Veins go away. And you just go.</h2>
			<p>Freeing your legs and getting back to the life you love starts now. <span class="looks_like_a_link">Learn more &gt;</span></p>
		</div>
		<img alt="Mountaineering" src="{$relative_path_to_root}images/slides/home_04.jpg">
	</a>
	<a href="/insurance">
		<div class="slide_overlay">
			<h2>More than 80% of treatments are covered.</h2>
			<p>To you, your problem may be all too obvious. We'll make it obvious to your insurance company, too. <span class="looks_like_a_link">Learn more &gt;</span></p>
		</div>
		<img alt="Camping" src="{$relative_path_to_root}images/slides/home_03.jpg">
	</a>
	<a href="/faq">
		<div class="slide_overlay">
			<h2>Free your legs from pain.</h2>
			<p>You can enjoy an active lifestyle again. Have more questions? <span class="looks_like_a_link">Check out our FAQ &gt;</span></p>
		</div>
		<img alt="Golfing" src="{$relative_path_to_root}images/slides/home_05.jpg">
	</a>
	<a href="/diagnosis_and_treatment">
		<div class="slide_overlay">
			<h2>Your legs tell us a lot. And you thought they didn't talk.</h2>
			<p>Relax. Your first visit to VCI is easy and fun, but so is every other time you come to see us. <span class="looks_like_a_link">Learn more &gt;</span></p>
		</div>
		<img alt="Cycling" src="{$relative_path_to_root}images/slides/home_02.jpg">
	</a>
	<a href="/faq">
		<div class="slide_overlay">
			<h2>Recovery is quick, but the benefits last a lifetime.</h2>
			<p>In fact, we encourage exercise immediately after your procedure. Have more questions? <span class="looks_like_a_link">Check out our FAQ &gt;</span></p>
		</div>
		<img alt="Running" src="{$relative_path_to_root}images/slides/home_01.jpg">
	</a>
</div>
HTML;
      break;
    }
    case 'static_photo_header':
    {
      global $unique_page_id;
      $output = <<<HTML
<section class="static_photo_header">
	<a href="#"><img alt="Header" src="{$relative_path_to_root}images/slides/$unique_page_id.jpg"></a>
</section>
HTML;
      break;
    }
    case 'nav_menu':
    {
      global $nav_highlighted_page;
      global $paths;
      $active = ' class="active"';
      $home_active = ($nav_highlighted_page == 'home') ? $active : '';
      $about_active = ($nav_highlighted_page == 'about') ? $active : '';
      $diagnosis_active = ($nav_highlighted_page == 'diagnosis') ? $active : '';
      $insurance_active = ($nav_highlighted_page == 'insurance') ? $active : '';
      $faq_active = ($nav_highlighted_page == 'faq') ? $active : '';
      $forms_active = ($nav_highlighted_page == 'forms') ? $active : '';
      $resources_active = ($nav_highlighted_page == 'resources') ? $active : '';
      $output = <<<HTML
<section class="nav_menu">
	<nav class="menu">
		<ul>
			<li><a href="{$paths['home']}"$home_active>Home</a></li>
			<li><a href="{$paths['about']}"$about_active>About VCI</a></li>
			<li><a href="{$paths['diagnosis']}"$diagnosis_active>Diagnosis &amp; Treatment</a></li>
			<li><a href="{$paths['insurance']}"$insurance_active>Insurance</a></li>
			<li><a href="{$paths['faq']}"$faq_active>FAQ</a></li>
			<li class="forms"><a href="#"$forms_active>Forms</a></li>
			<li class="resources"><a href="#"$resources_active>Resources</a></li>
		</ul>
	</nav>
</section>
HTML;
      break;
    }
    case 'nav_strip':
    {
      global $nav_highlighted_page;
      global $paths;
      $active = ' class="active"';
      $home_active = ($nav_highlighted_page == 'home') ? $active : '';
      $about_active = ($nav_highlighted_page == 'about') ? $active : '';
      $diagnosis_active = ($nav_highlighted_page == 'diagnosis') ? $active : '';
      $insurance_active = ($nav_highlighted_page == 'insurance') ? $active : '';
      $faq_active = ($nav_highlighted_page == 'faq') ? $active : '';
      $forms_active = ($nav_highlighted_page == 'forms') ? $active : '';
      $resources_active = ($nav_highlighted_page == 'resources') ? $active : '';
      $output = <<<HTML
<div id="nav_strip_position_capture"></div>
<section class="nav_strip">
	<div class="width_limiter">
		<div class="block_container">
			<nav class="strip full">
				<ul>
					<li><a href="{$paths['home']}"$home_active>Home</a></li>
					<li><a href="{$paths['about']}"$about_active>About VCI</a></li>
					<li><a href="{$paths['diagnosis']}"$diagnosis_active>Diagnosis &amp; Treatment</a></li>
					<li><a href="{$paths['insurance']}"$insurance_active>Insurance</a></li>
					<li><a href="{$paths['faq']}"$faq_active>FAQ</a></li>
					<li class="forms"><a href="#"$forms_active>Forms</a></li>
					<li class="resources"><a href="#"$resources_active>Resources</a></li>
				</ul>
			</nav>
		</div>
	</div>
</section>
HTML;
      break;
    }
    case 'resources_rollout':
    {
      $class = 'resources rollout ' . $options['location'];
      $close_button = ($options['location'] == 'side') ? '<a class="close_button" href="#" title="Close"></a>' : '';
      $shade = ($options['location'] == 'side') ? '<div class="rollout_shade resources"></div>' : '';
      $summand_1 = rand(1, 4);
      $summand_2 = rand(1, 4);
      $sum_trick = $summand_1 + $summand_2 + 1;
      $output = <<<HTML
$shade<section class="$class">
	<div class="width_limiter">
		<div class="block_container" style="overflow: visible;">$close_button
			<form id="appointment" name="appointment" method="post" data-action="/ajax/log_and_send_email.php">
				<h3>Make an Appointment</h3>
				<input type="text" name="name" class="name" placeholder="Name">
				<input type="email" name="email" class="email" placeholder="Email">
				<input type="tel" name="phone" class="phone" placeholder="Phone Number">
				<select name="reason">
					<option>Reason for Appointment</option>
					<option>Spider Veins</option>
					<option>Varicose Veins</option>
					<option>Leg Ulcers</option>
					<option>Leg Aching</option>
					<option>Leg Burning</option>
					<option>Leg Heaviness</option>
					<option>Throbbing of Leg</option>
					<option>Previous or Current DVT</option>
					<option>Other</option>
				</select>
				<select name="preferred_contact">
					<option>Preferred Contact Method</option>
					<option>Phone</option>
					<option>Email</option>
				</select>
				<input name="jdarc" class="jdarc" type="text" placeholder="Please enter the answer to $summand_1 + $summand_2" autocomplete="off">
				<input name="st" class="st" type="hidden" value="$sum_trick">
				<input name="email_type" class="email_type" value="appointment" type="hidden">
				<a href="#" class="button negative submit" data-form-id="appointment" data-processing-text="Submitting…" data-processed-text="Request Submitted">Submit</a>
			</form>
			<h3>Directions to Our Office</h3>
			<input type="text" name="address" class="address" placeholder="Address">
			<a href="#" class="button negative get_directions">Get Directions</a>
			<input type="text" name="search" class="search" placeholder="Search">
		</div>
	</div>
	<div class="prop_open"></div>
</section>
HTML;
      break;
    }
    case 'forms_rollout':
    {
      $class = 'forms rollout ' . $options['location'];
      $close_button = ($options['location'] == 'side') ? '<a class="close_button" href="#" title="Close"></a>' : '';
      $shade = ($options['location'] == 'side') ? '<div class="rollout_shade forms"></div>' : '';
      $output = <<<HTML
$shade<section class="$class">
	<div class="width_limiter">
		<div class="block_container" style="overflow: visible;">$close_button
			<h3>Download Forms</h3>
			<select name="form" class="form_list">
				<option>Select Form</option>
				<optgroup label="New Patient Forms">
					<option value="New_Patient_Paper_Work.pdf">New Patient Packet</option>
				</optgroup>
			</select>
		</div>
	</div>
	<div class="prop_open"></div>
</section>
HTML;
      break;
    }
    case 'footer':
    {
      $year = date('Y');
      $summand_1 = rand(1, 4);
      $summand_2 = rand(1, 4);
      $sum_trick = $summand_1 + $summand_2 + 1;
      $output = <<<HTML
<footer class="main">
  <div class="width_limiter">
    <div class="block_container">
      <div class="col1">
        <form id="message"
              name="message"
              method="post"
              data-action="/ajax/log_and_send_email.php">
          <h3>Let's Talk</h3>
          <input name="name" class="name" type="text" placeholder="Name">
          <input name="phone"
                 class="phone"
                 type="tel"
                 placeholder="Phone Number">
          <input name="email"
                 class="email"
                 type="email"
                 placeholder="Email Address">
          <input name="jdarc"
                 class="jdarc"
                 type="text"
                 placeholder="Please enter the answer to $summand_1 + $summand_2"
                 autocomplete="off">
          <input name="st" class="st" type="hidden" value="$sum_trick">
          <input name="message" class="message" type="hidden">
          <input name="email_type"
                 class="email_type"
                 value="message"
                 type="hidden">
          <textarea name="message_1"
                    class="message_1"
                    placeholder="Message"></textarea>
        </form>
        <h3>Call Us</h3>
        <p>We would love to answer any questions you may have, get you set up
          for an office tour, or schedule an appointment so we can help you free
          your legs.
        </p>
        <ul>
          <li><span class="leader">Phone</span> 303 798 3467</li>
        </ul>
        <div class="social">
          <a href="https://www.facebook.com/veincareinstitute" class="fb" target="_blank"></a>
          <a href="https://www.instagram.com/veincareinstitute/" class="ig" target="_blank"></a>
        </div>
      </div>
      <div class="col2">
        <div class="h3_spacer">&nbsp;</div>
        <textarea name="message_2"
                  class="message_2"
                  placeholder="Message"></textarea>
        <a href="#"
           class="button negative submit"
           data-form-id="message"
           data-processing-text="Submitting…"
           data-processed-text="Message Sent">Send Message</a>
        <h3>Stop By</h3>
        <p>Stop by for a tour of the facility, to ask any questions you may
          have, to schedule an appointment, or just to say hello!
        </p>
        <h3>Belmar Location</h3>
        <p>7390 West Alameda Ave.<br>Lakewood, CO 80226</p>
      </div>
      <div class="col3">
        <div class="h3_spacer">&nbsp;</div>
        <blockquote>There really is no reason to wait. Our advanced vein
          treatments are not painful and, as the facts show, things will just
          get worse over time. Come in and see the vein care experts. Get your
          legs healthy and get them out in public again.
        </blockquote>
        <a href="#" class="appointment button negative">Make an Appointment</a>
      </div>
      <p class="copyright">©$year Vein Care Institute</p>
    </div>
  </div>
</footer>
HTML;
      break;
    }
  }
  return $output;
}

function echo_section($which, $options = array()) {
  echo(get_section($which, $options));
}
