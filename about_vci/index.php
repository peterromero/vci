<?php
$absolute_path_to_root = $_SERVER['DOCUMENT_ROOT'] . '/';
$relative_path_to_root = '../';
$nav_highlighted_page = 'about';
$unique_page_id = 'about';
require_once("{$absolute_path_to_root}templating.php");
open_document();
echo_head_section(array(
  'title' => 'About VCI:',
  'description' => 'About the Vein Care Institute and our staff.',
  'keywords' => 'vein care, varicose veins, endovenous laser therapy, spider veins, vascular surgery, sclerotherapy'
));
?>
  <body class="page_<?php echo($unique_page_id); ?>">
    <?php echo_section('masthead'); ?>
    <?php echo_section('nav_menu'); ?>
    <?php echo_section('resources_rollout', array('location' => 'top')); ?>
    <?php echo_section('forms_rollout', array('location' => 'top')); ?>
    <?php echo_section('static_photo_header'); ?>
    <?php echo_section('nav_strip'); ?>
    <?php echo_section('forms_rollout', array('location' => 'side')); ?>
    <?php echo_section('resources_rollout', array('location' => 'side')); ?>
    <section class="gray bottom_layer first">
      <div class="width_limiter">
        <div class="block_container">
          <div class="half">
            <h2>Overview</h2>
            <p>At The Vein Care Institute, we want to help you free your legs
              and get back into your life! One of the best parts about living in
              Colorado is an abundance of opportunities to enjoy an active
              lifestyle. But when vein disorders strike, getting out and getting
              active can be difficult at best, and downright painful at worst.
            </p>
            <p>But it doesn’t have to be that way. Once we set you on the path
              toward recovery, you can do anything you want to do without the
              pain or embarrassment caused by vein issues in your legs. That’s
              why we designed our state-of-the-art facility with one goal in
              mind: to provide comprehensive diagnosis and treatment for all
              types vein disorders. We believe that with our dedication, quality
              care and cutting-edge technology, no one should have to put an
              active lifestyle on hold because of vein disorders!
            </p>
            <p class="long_bottom">The Vein Care Institute has received a
              national accreditation from the Intersocietal Accreditation
              Commission (IAC) for vein center certification. This translates
              into a national "seal of approval" for safety, expertise,
              competence and standards of care for venous disease.
            </p>
          </div>
          <div class="half">
            <h2>Our History</h2>
            <p>The Vein Care Institute is a one-of-a-kind facility wherein
              technology, training, skill, and heart combine to provide patients
              with the best vein care available. Before opening VCI, Dr. Schuh
              spent nearly the first decade of his General and Vascular Surgery
              career performing hundreds of traditional vein stripping
              procedures which required a general anesthesia, resulted in a fair
              amount of trauma, a prolonged recovery and unpredictable results.
              In 2006, Dr. Schuh and colleague, Dr. Dana Mann, an Interventional
              Radiologist, began performing the tremendously more effective
              endovenous laser ablation procedures. This new technology combined
              with ultrasound guidance provided such immediate, effective, and
              long lasting results, in 2007 the Vein Care Institute was created
              to focus solely on vein disease treatment by laser. VCI is located
              in the Belmar shopping area in Lakewood. Dr. Darrick Payne joined
              the practice in 2009. Dr. Schuh was joined by another vascular
              surgeon, Dr. Brian Ridge, in March of 2010. The majority of the
              vein ablation procedures done at VCI are performed by our two
              vascular surgeons.
            </p>
          </div>
        </div>
      </div>
    </section>
    <section class="white top_layer">
      <div class="width_limiter">
        <div class="block_container">
          <h2 class="full long_top">Get to Know Our Team</h2>
          <?php

          class employee {
            var $display_name;
            var $image_name;
            var $weight;
            var $description;
            var $group_index;

            function __construct($display_name, $image_name, $group_index, $weight, $description) {
              $this->display_name = $display_name;
              $this->image_name = $image_name;
              $this->group_index = $group_index;
              $this->weight = $weight;
              $this->description = $description;
            }
          }

          $employees = array();
          // Employees don't have to be entered in any specific order. They will be sorted first by group_index, then by weight.
          // Each time the group_index changes, a new row begins.
          $employees[] = new employee("William Schuh, M.D.", "William.jpg", 10, 10, "Dr. William Schuh received his medical degree from the University of Colorado in 1994, followed with a General Surgery residency in Phoenix at Good Samaritan Hospital, and then a Vascular Surgery fellowship at Saint Louis University. Dr. Schuh has been in practice in the Denver area since 2000, is board-certified in General and Vascular Surgery, a Fellow of the American College of Surgeons (F.A.C.S.), and a member of the American College of Phlebology. A Colorado native, Dr. Schuh is proud to have served in the United States Marine Corps, enjoys spending time outdoors, and loves living in the mountains with his family.");
          $employees[] = new employee("Brian Ridge, M.D.", "Brian.jpg", 10, 20, "Dr. Brian Ridge received his medical degree from the University of Utah in 1983. He completed his general surgery residency at Harvard (Massachusetts General Hospital) in 1988 and his vascular surgery fellowship at Harvard in 1990. He has been board certified in general surgery since 1990, and became board certified in vascular surgery in 1991. Beginning in 1990, Dr. Ridge practiced general and vascular surgery at St. Anthony and Lutheran hospitals. In 2010 Dr. Ridge joined VCI when he developed a special interest in venous disease. He also concentrated his inpatient practice at St. Anthony hospital on providing anterior spine exposure for cervical, thoracic, and lumbar spinal fusion procedures. Dr. Ridge is a fellow of the American College of Surgeons.");

          $employees[] = new employee('Lisa Defeyter', 'Lisa.jpg', 20, 'lisa', '<span class="position_title">Practice Administrator</span>');

          $employees[] = new employee('Mel', 'Melissa.jpg', 30, 'mel', '<span class="position_title">RDMS/RVT</span>');
          $employees[] = new employee('Emily', 'Emily.jpg', 30, 'emily', '<span class="position_title">RDMS/RVT</span>');
          $employees[] = new employee('Beth', 'Beth.jpg', 30, 'beth', '<span class="position_title">RVT</span>');
          $employees[] = new employee('Amy', 'Amy.jpg', 30, 'amy', '<span class="position_title">Medical Assistant/Sclerotherapist</span>');
          $employees[] = new employee('Itsel', 'Itsel.jpg', 30, 'itsel', '<span class="position_title">Medical Assistant/PRP Specialist</span>');
          $employees[] = new employee('Kim', 'Kim.jpg', 30, 'kim', '<span class="position_title">Medical Assistant</span>');
          $employees[] = new employee('Alberta', 'Alberta.jpg', 30, 'alberta', '<span class="position_title">Medical Assistant</span>');
          $employees[] = new employee('Kariann', 'Kariann.jpg', 30, 'kariann', '<span class="position_title">LPN/Sclerotherapist</span>');
          $employees[] = new employee('Marissa', 'Marissa.jpg', 30, 'marissa', '<span class="position_title">Ultrasonographer/Administrative Assistant</span>');

          $employees[] = new employee('Janelle', 'Janelle.jpg', 40, 'janelle', '<span class="position_title">Eligibility Specialist</span>');
          $employees[] = new employee('Cheryl', 'Cheryl.jpg', 40, 'cheryl', '<span class="position_title">Patient Care Coordinator</span>');
          $employees[] = new employee('Deena', 'Deena.jpg', 40, 'deena', '<span class="position_title">Authorization Specialist</span>');

          $employees[] = new employee('Aliya', 'Aliya.jpg', 50, 'aliya', '<span class="position_title">Front Office</span>');
          $employees[] = new employee('Nicole', 'Nicole.jpg', 50, 'nicole', '<span class="position_title">Front Office Liaison</span>');
          $employees[] = new employee('Rahjene', 'Rahjene.jpg', 50, 'rahjene', '<span class="position_title">Front Office Liaison</span>');
          $employees[] = new employee('Nili', 'Nili.jpg', 50, 'nili', '<span class="position_title">Office Coordinator</span>');
          $employees[] = new employee('Stephanie', 'Stephanie.jpg', 50, 'stephanie', '<span class="position_title">Billing</span>');

          usort($employees, function ($a, $b) {
            if ($a->group_index == $b->group_index) {
              if ($a->weight == $b->weight) {
                return 0;
              }
              return ($a->weight < $b->weight) ? -1 : 1;
            }
            else {
              return ($a->group_index < $b->group_index) ? -1 : 1;
            }
          });
          $last_group_index = null;
          $column_index = -1;
          $row_clear = ' style="clear: both;"';
          foreach ($employees as $e) {
            $column_index++;
            if ($column_index > 2) {
              $column_index = 0;
            }
            if ($e->group_index != $last_group_index) {
              $column_index = 0;
            }
            if ($column_index == 0) {
              $clear = $row_clear;
            }
            else {
              $clear = '';
            }
            $last_group_index = $e->group_index;
            echo <<<HTML
<div class="third employee"$clear>
	<h3 class="arrow">{$e->display_name}</h3>
	<div class="images">
		<img alt="{$e->display_name}" class="fill" src="/images/staff/{$e->image_name}">
	</div>
	<p class="long_bottom">{$e->description}</p>
</div>
HTML;
          }
          ?>
        </div>
      </div>
    </section>
    <section class="gray bottom_layer">
      <div class="width_limiter">
        <div class="block_container">
          <h2 class="full long_top">Life-Changing Stories</h2>
          <p class="two-thirds">See stories from people just like you who made
            the decision to free their legs. Hear how their lives were changed,
            and watch as they show you how they got back to the life they love.
          </p>
          <div class="video_area half">
            <div class="video">
              <iframe src="//player.vimeo.com/video/31668091?title=0&amp;byline=0&amp;portrait=0&amp;color=00aec5"
                      webkitallowfullscreen
                      mozallowfullscreen
                      allowfullscreen></iframe>
            </div>
            <p class="caption">
              <span class="leader">Mike Kramer</span> EVLA and Sclerotherapy
            </p>
          </div>
          <div class="video_area half">
            <div class="video">
              <iframe src="//player.vimeo.com/video/25241471?title=0&amp;byline=0&amp;portrait=0&amp;color=00AEC5"
                      webkitallowfullscreen
                      mozallowfullscreen
                      allowfullscreen></iframe>
            </div>
            <p class="caption"><span class="leader">Michele Elliot</span> EVLA
            </p>
          </div>
          <div class="video_area half">
            <div class="video">
              <iframe src="//player.vimeo.com/video/25116168?title=0&amp;byline=0&amp;portrait=0&amp;color=00AEC5"
                      webkitallowfullscreen
                      mozallowfullscreen
                      allowfullscreen></iframe>
            </div>
            <p class="caption">
              <span class="leader">Mike Cohen</span> EVLA and Sclerotherapy
            </p>
          </div>
          <div class="video_area half">
            <div class="video">
              <iframe src="//player.vimeo.com/video/25101704?title=0&amp;byline=0&amp;portrait=0&amp;color=00AEC5"
                      webkitallowfullscreen
                      mozallowfullscreen
                      allowfullscreen></iframe>
            </div>
            <p class="caption"><span class="leader">Katie Schaefer</span> EVLA
            </p>
          </div>
          <div class="video_area half">
            <div class="video">
              <iframe src="//player.vimeo.com/video/25118258?title=0&amp;byline=0&amp;portrait=0&amp;color=00AEC5"
                      webkitallowfullscreen
                      mozallowfullscreen
                      allowfullscreen></iframe>
            </div>
            <p class="caption">
              <span class="leader">Keith Burke</span> EVLA and Sclerotherapy
            </p>
          </div>
          <div class="video_area half">
            <div class="video">
              <iframe src="//player.vimeo.com/video/25140695?title=0&amp;byline=0&amp;portrait=0&amp;color=00AEC5"
                      webkitallowfullscreen
                      mozallowfullscreen
                      allowfullscreen></iframe>
            </div>
            <p class="caption">
              <span class="leader">Mats and Andrea Andersson</span> EVLA and
              Sclerotherapy
            </p>
          </div>
        </div>
      </div>
    </section>
    <?php echo_section('footer'); ?>
  </body>
<?php close_document();
