<?php
require_once('pdo.php');
function search($needle_list) {
	$output = '';
	global $db;
	$needles = preg_split('/ /', $needle_list);
	$where_conditions = array();
	$needles_with_percents_for_like_statements = array();
	$matches = array();
	foreach ($needles as $key => $needle) {
		$needle = preg_replace('/[^\w ]/', '', strtolower($needle));
		if (strlen($needle) > 2) {
			$needles_with_percents_for_like_statements[] = "%$needle%";
			$where_conditions[] = "terms like ?";
		} else {
			unset($needles[$key]);
		}
	}
	if (count($needles)) {
		$q_str = "select `page_title`, `page_url`, `fulltext` from search_index where " . implode(" and ", $where_conditions);
		try {
			$q = $db->prepare($q_str);
			$q->execute($needles_with_percents_for_like_statements);
		} catch(PDOException $e) {
			echo "Error: " . $e->getMessage();
		}
		while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
			foreach ($needles as $needle) {
				// This searches for words beginning with the needle.
				$search_pattern = "/\b(" . preg_replace('/(.)(?=.)/', '\\1[^\w ]*', $needle) . ")/i";
				$row['fulltext'] = preg_replace($search_pattern, "<b>\\1</b>", $row['fulltext'], -1, $number_of_occurrences);
			}
			$first_position_of_any_needle = strpos($row['fulltext'], '<');
			$beginning_truncation_position = $first_position_of_any_needle - 20;
			if ($beginning_truncation_position < 0) $beginning_truncation_position = 0;
			$row['fulltext'] = substr($row['fulltext'], $beginning_truncation_position);
			$row['fulltext'] = substr($row['fulltext'], strpos($row['fulltext'], ' ') + 1, 200);
			$row['fulltext'] = substr($row['fulltext'], 0, strrpos($row['fulltext'], ' '));
			$row['fulltext'] = "…" . str_replace(array("<b>", "</b>"), array("<span style='font-weight: bold;'>", "</span>"), $row['fulltext']) . "…";
			$matches[] = array(
				'text' => "<h3><a href=\"{$row['page_url']}\">{$row['page_title']}</a></h3><p>{$row['fulltext']}</p><p class='subtext after_p'>$number_of_occurrences matches</p>",
				'number_of_occurrences' => $number_of_occurrences
			);
		}
		usort($matches, function($a, $b) {
			return $a['number_of_occurrences'] < $b['number_of_occurrences'];
		});
		foreach($matches as $match) {
			$output .= $match['text'];
		}
	}
	return $output;
}
