var vci_app = {
	nav_strip_top: 0,
	has_charts: false,
	charts: {
		0: {top: 0, height: 0},
		1: {top: 0, height: 0},
		2: {top: 0, height: 0},
		3: {top: 0, height: 0}
	},
	scroll_bottom: function() {
		return $(window).scrollTop() + $(window).height();
	},
	vertical_layout: function() {
		if ($('#nav_strip_position_capture').length) {
			vci_app.nav_strip_top = $('#nav_strip_position_capture').offset().top;
		} else {
			vci_app.nav_strip_top = 0;
		}
		if ($('.chart').length) {
			vci_app.has_charts = true;
			vci_app.charts[0].top = $('.chart').eq(0).offset().top;
			vci_app.charts[1].top = $('.chart').eq(1).offset().top;
			vci_app.charts[2].top = $('.chart').eq(2).offset().top;
			vci_app.charts[3].top = $('.chart').eq(3).offset().top;
			vci_app.charts[0].height = $('.chart').eq(0).height();
			vci_app.charts[1].height = $('.chart').eq(1).height();
			vci_app.charts[2].height = $('.chart').eq(2).height();
			vci_app.charts[3].height = $('.chart').eq(3).height();
		}
	}
};
var window_width;
$(window).resize(function() {
	if ($(window).width() != window_width) { // Why? Because iOS.
		adjust_nav_strip_link_spacing();
		if ($('section.nav_menu').hasClass('open')) {
			set_nav_menu_display('hide', 'immediate');
		}
		if ($('section.resources.rollout.side').hasClass('open')) {
			set_rollout_menu_display('resources', 'side', 'hide', 'immediate');
			set_rollout_menu_display('forms', 'side', 'hide', 'immediate');
		}
		resize_charts();
		resize_static_photo_header();
		update_dev_toolbar();
		vci_app.vertical_layout();
		window_width = $(window).width();
	}
});
$(window).scroll(function() {
	if ($(window).scrollTop() > vci_app.nav_strip_top) {
		$('section.nav_strip').addClass('locked');
		$('#nav_strip_position_capture').addClass('locked');
	} else {
		$('section.nav_strip').removeClass('locked');
		$('#nav_strip_position_capture').removeClass('locked');
	}
});
$(function() {
	window_width = $(window).width();
	$('input, textarea').placeholder();
	$('select').selectBoxIt({
		autoWidth: false,
		showFirstOption: false,
		showEffect: 'slideDown',
		showEffectSpeed: 200,
		hideEffect: 'slideUp',
		hideEffectSpeed: 200
	});
	$('.form_list').change(function() {
		var win = window.open('/downloads/' + $(this).val(), '_blank');
	});
	adjust_nav_strip_link_spacing();
	position_side_rollout('resources');
	position_side_rollout('forms');
	resize_charts();
	resize_static_photo_header();
	update_dev_toolbar();
	$('section.masthead.narrow .hamburger_toggle').click(function(e) {
		if ($('section.nav_menu').hasClass('open')) {
			set_nav_menu_display('hide', 'animate');
		} else {
			set_nav_menu_display('show', 'animate');
		}
		e.preventDefault();
	});
	$('nav.menu .resources a').click(function(e) {
		if ($('section.resources.rollout.top').hasClass('open')) {
			set_rollout_menu_display('resources', 'top', 'hide', 'animate');
		} else {
			set_rollout_menu_display('forms', 'top', 'hide', 'immediate');
			set_rollout_menu_display('resources', 'top', 'show', 'animate');
		}
		e.preventDefault();
	});
	$('nav.menu .forms a').click(function(e) {
		if ($('section.forms.rollout.top').hasClass('open')) {
			set_rollout_menu_display('forms', 'top', 'hide', 'animate');
		} else {
			set_rollout_menu_display('resources', 'top', 'hide', 'immediate');
			set_rollout_menu_display('forms', 'top', 'show', 'animate');
		}
		e.preventDefault();
	});
	$('nav.strip .resources a').click(function(e) {
		if ($('section.resources.rollout.side').hasClass('open')) {
			set_rollout_menu_display('resources', 'side', 'hide', 'animate');
		} else {
			set_rollout_menu_display('resources', 'side', 'show', 'animate');
		}
		e.preventDefault();
	});
	$('nav.strip .forms a').click(function(e) {
		if ($('section.forms.rollout.side').hasClass('open')) {
			set_rollout_menu_display('forms', 'side', 'hide', 'animate');
		} else {
			set_rollout_menu_display('forms', 'side', 'show', 'animate');
		}
		e.preventDefault();
	});
	$('a.button').click(function(e) {
		e.preventDefault();
	});
	$('.get_directions').click(function() {
		var saddr = $(this).siblings('.address').val().replace(/[^\w ]/g, '').replace(/[ ]+/g, '+');
		var url = 'http://maps.google.com/maps?f=d&saddr=' + saddr + '&daddr=7309+West+Alameda+Ave+80226';
		window.open(url);
	});
	$('.address').keyup(function(e) {
		if (e.which == 13) {
			$(this).siblings('.get_directions').click();
		}
	});
	$('.resources.rollout.side .close_button').click(function(e) {
		set_rollout_menu_display('resources', 'side', 'hide', 'animate');
		e.preventDefault();
	});
	$('.forms.rollout.side .close_button').click(function(e) {
		set_rollout_menu_display('forms', 'side', 'hide', 'animate');
		e.preventDefault();
	});
	$('.rollout_shade').click(function() {
		set_rollout_menu_display('resources', 'side', 'hide', 'animate');
		set_rollout_menu_display('forms', 'side', 'hide', 'animate');
	});
	$('.rollout_trigger').click(function(e) {
		rollout_toggle($(this));
		e.preventDefault();
	});
	$('.rollout_close').click(function(e) {
		if (window.innerWidth > 768) {
			var $rollout_trigger = $('.rollout_trigger.opener[data-rollout-id=' + $(this).parents('.rollout_content').attr('data-rollout-id') + ']');
		} else {
			var $rollout_trigger = $('.rollout_trigger.closer[data-rollout-id=' + $(this).parents('.rollout_content').attr('data-rollout-id') + ']');
		}
		rollout_toggle($rollout_trigger);
		e.preventDefault();
	});
	$('.rollout_toggle_clicker').click(function(e) {
		if (window.innerWidth > 768) {
			var $rollout_trigger = $('.rollout_trigger.opener[data-rollout-id=' + $(this).attr('data-rollout-id') + ']');
		} else {
			var $rollout_trigger = $('.rollout_trigger.closer[data-rollout-id=' + $(this).attr('data-rollout-id') + ']');
		}
		rollout_toggle($rollout_trigger);
		e.preventDefault();
	});
	$('footer.main .appointment.button').click(function() {
		open_appointment_form();
	});
	// Reconcile the two textareas in footer (only one shows at a time because of code order/columns & responsive design). The actual value passed to the form is stored in a hidden field.
	$('textarea.message_1, textarea.message_2').keyup(function() {
		$('.message').val($(this).val());
		if ($(this).hasClass('message_1')) {
			$('.message_2').val($(this).val());
		}
		if ($(this).hasClass('message_2')) {
			$('.message_1').val($(this).val());
		}
	});
	$('.search').keyup(function(e) {
		if (e.which == 13) {
			window.location.href = '/search.php?q=' + encodeURIComponent($(this).val());
		}
	});
	$('.button.submit').click(function() {
		$button = $(this);
		$button.addClass('processing').text($button.attr('data-processing-text'));
		var $form = $('form#' + $button.attr('data-form-id'));
		$.post(
			$form.attr('data-action'),
			$form.serialize(),
			function(data) {
				data = $.parseJSON(data);
				// If there exists a p with class "form_status_console" and attribute "data-form-id" set to the id of the pertinent form,
				// status messages (error or otherwise) will always be put there. If this p does not exist, error messages from the
				// submission will be alerted — but not non-error messages.
				//
				// In practice, this is used to show more comprehensive form submission console results on the admin side while not
				// cluttering up the front end with lots of extra blocks of content.
				console.log({data: data});
				if ($('.form_status_console[data-form-id=' + $button.attr('data-form-id') + ']').length) {
					var a = 'Submission result: ' + data.status;
					if (data.hasOwnProperty('messages')) {
						$.each(data.messages, function(key, value) {
							a = a + '<br>' + value;
						});
					}
					$('.form_status_console[data-form-id=' + $button.attr('data-form-id') + ']').html(a);
				} else {
					if (data === null || data.status == 'error') {
						var a = 'There was one or more error(s) with your submission:\n';
						if (data !== null && data.hasOwnProperty('errors')) {
							$.each(data.errors, function(key, value) {
								a = a + '\n' + value;
							});
						}
						alert(a + '\n\nPlease try your submission again. If the problem persists, please contact us by phone.');
					}
				}
				$button.removeClass('processing').addClass('processing_complete_burst');
				window.setTimeout(
					function() {
						// Also, if the .submit.button has an attribute called "data-processed-text", the button's text will be set to
						// that value after the form submission is complete.
						$button.text($button.attr('data-processed-text')).removeClass('processing_complete_burst').addClass('processing_complete');
						window.setTimeout(function() {
							$button.removeClass('processing_complete');
						}, 1000
					);
					}, 200
				);
			}
		);
	});
});
function get_responsive_mode() {
	var output = 'full_width';
	if ($('footer .appointment.button').css('position') == 'relative') {output = 'single_column';}
	if ($('section.masthead').css('display') == 'none') {output = 'mobile';}
	return output;
}
function resize_static_photo_header() {
	if ($('.static_photo_header').length) {
		var mode = get_responsive_mode();
		switch (get_responsive_mode()) {
			case 'single_column':
			case 'mobile': {
				var effective_width = $('.static_photo_header').width();
				var desired_height = effective_width / 1.92; /* Aspect ratio: 960 × 500 */
				var scaled_image_width = desired_height * 5.12 /* Aspect ratio: 2560 × 500 */
				$('.static_photo_header, .static_photo_header img').height(desired_height);
				$('.static_photo_header a').css('margin-left', -scaled_image_width/2);
				break;
			}
			case 'full_width': {
				$('.static_photo_header, .static_photo_header img').height(500);
				$('.static_photo_header a').css('margin-left', -1280);
				break;
			}
		}
		vci_app.vertical_layout();
	}
}
function open_appointment_form() {
	if (get_responsive_mode() == 'mobile') {
		set_nav_menu_display('show', 'immediate');
		set_rollout_menu_display('resources', 'top', 'show', 'immediate');
		var scroll_goal = $('.resources.rollout.top').offset().top;
		var callback_called = false;
		$('html, body').animate({
			scrollTop: scroll_goal
		},
		500,
		function() {
			if (!callback_called) {
				callback_called = true;
				$('.resources.rollout.top .name').focus();
			}
		});
	} else {
		set_rollout_menu_display('resources', 'side', 'show', 'animate');
		var scroll_goal = $('.resources.rollout.side').offset().top;
		var callback_called = false;
		$('html, body').animate({
			scrollTop: scroll_goal
		},
		500,
		function() {
			if (!callback_called) {
				callback_called = true;
				$('.resources.rollout.side .name').focus();
			}
		});
	}
}
function rollout_toggle($trigger) {
	if (!$trigger.hasClass('animating')) { //Don't run the rollout if a previous one is already running.
		if ($trigger.hasClass('open')) {
			_rollout_close($trigger);
		} else {
			if ($('.rollout_trigger.open').length) { //Close open rollouts before opening a new one.
				_rollout_close($('.rollout_trigger.open'), function() {
					_rollout_open($trigger);
				});
			} else {
				_rollout_open($trigger);
			}
		}
	}
}
function _rollout_toggle_text($trigger) {
	if ($trigger.attr('data-toggle-text')) {
		var swap = $trigger.attr('data-toggle-text');
		$trigger.attr('data-toggle-text', $trigger.text()).text(swap);
	}
}
function _rollout_close($trigger, callback) {
	_rollout_toggle_text($trigger);
	$trigger.removeClass('open').addClass('animating');
	var $content = $('.rollout_content[data-rollout-id=' + $trigger.attr('data-rollout-id') + ']');
	$content
		.css('opacity', 0.4)
		.slideUp(
			800,
			function() {
				$trigger.removeClass('animating');
				if (typeof(callback) == 'function') {callback();}
			}
		)
	;
}
function _rollout_open($trigger, callback) {
	_rollout_toggle_text($trigger);
	$trigger.addClass('open animating');
	var $content = $('.rollout_content[data-rollout-id=' + $trigger.attr('data-rollout-id') + ']');
	$content .slideDown(800, function() {
		$content.css('opacity', 1);
		$trigger.removeClass('animating');
		var extra_space_for_nav_bar = (window.innerWidth > 768)?94:0;
		var scroll_goal = $content.offset().top - extra_space_for_nav_bar;
		$('html, body').animate(
			{scrollTop: scroll_goal},
			500
		).promise().done(function() {
			if (typeof(callback) == 'function') {callback();}
		});
	});
}
function set_nav_menu_display(show_or_hide, how) {
	switch(show_or_hide) {
		case 'show': {
			switch(how) {
				case 'animate': {
					$('section.nav_menu').slideDown(300).addClass('open');
					break;
				}
				case 'immediate': {
					$('section.nav_menu').show().addClass('open');
					break;
				}
			}
			vci_app.vertical_layout();
			break;
		}
		case 'hide': {
			switch(how) {
				case 'animate': {
					$('section.nav_menu').slideUp(300).removeClass('open');
					break;
				}
				case 'immediate': {
					$('section.nav_menu').hide().removeClass('open');
					break;
				}
			}
			set_rollout_menu_display('resources', 'top', 'hide', how);
			vci_app.vertical_layout();
			break;
		}
	}
}
function set_rollout_menu_display(which, position, show_or_hide, how) {
	var nav_menu_li_class_name = '.' + which;
	var rollout_section_class_name = '.' + which + '.rollout';
	var rollout_shade_class_name = '.' + which + '.rollout_shade';
	switch (position) {
		case 'top': {
			switch(show_or_hide) {
				case 'show': {
					$('nav.menu ' + nav_menu_li_class_name).addClass('open');
					if (which == 'forms') {
						$('section.nav_menu li.resources').hide();
					}
					switch(how) {
						case 'animate': {
							$('section' + rollout_section_class_name + '.top').slideDown(300).addClass('open');
							break;
						}
						case 'immediate': {
							$('section' + rollout_section_class_name + '.top').show().addClass('open');
							break;
						}
					}
					break;
				}
				case 'hide': {
					if (which == 'forms') {
						$('section.nav_menu li.resources').show();
					}
					switch(how) {
						case 'animate': {
							$('section' + rollout_section_class_name + '.top').slideUp(300, function() {$('nav.menu ' + nav_menu_li_class_name).removeClass('open');}).removeClass('open');
							break;
						}
						case 'immediate': {
							$('section' + rollout_section_class_name + '.top').hide().removeClass('open');
							$('nav.menu ' + nav_menu_li_class_name).removeClass('open');
							break;
						}
					}
					break;
				}
			}
			vci_app.vertical_layout();
			break;
		}
		case 'side': {
			switch(show_or_hide) {
				case 'show': {
					position_side_rollout(which);
					switch(how) {
						case 'animate': {
							$(rollout_shade_class_name).fadeIn(300);
							$('section' + rollout_section_class_name + '.side').fadeIn(300).addClass('open');
							break;
						}
						case 'immediate': {
							$(rollout_shade_class_name).show();
							$('section' + rollout_section_class_name + '.side').show().addClass('open');
							break;
						}
					}
					break;
				}
				case 'hide': {
					switch(how) {
						case 'animate': {
							$(rollout_shade_class_name).fadeOut(300);
							$('section' + rollout_section_class_name + '.side').fadeOut(300).removeClass('open');
							break;
						}
						case 'immediate': {
							$(rollout_shade_class_name).hide();
							$('section' + rollout_section_class_name + '.side').hide().removeClass('open');
							break;
						}
					}
					break;
				}
			}
			break;
		}
	}
}
function update_dev_toolbar() {
	if ($('html').hasClass('dev')) {
		var $toolbar = $('.dev_toolbar');
		var client_width = document.body.clientWidth;
		var client_height = document.body.clientHeight;
		var css_width = window.innerWidth;
		var css_height = window.innerHeight;
		var jq_width = $(window).width();
		var jq_height = $(window).height();
		var text = 'CSS window size: ' + css_width + ' × ' + css_height + ' &nbsp;/&nbsp; jQuery window size: ' + jq_width + ' × ' + jq_height + ' &nbsp;/&nbsp; Entire page size: ' + client_width + ' × ' + client_height;
		$toolbar.html(text);
	}
}
function adjust_nav_strip_link_spacing() {
	var nav_width = $('nav.strip').width();
	var content_width = 0;
	var $nav_links = $('nav.strip li');
	var num_links = $nav_links.length;
	$nav_links.each(function() {
		content_width += $(this).width();
	});
	var link_gutter_width = Math.floor((nav_width - content_width) / (num_links - 1)) - 6; //The final -6 is a failsafe to avoid the line breaking. If browsers were 100% mathematically accurate, it might not be necessary. I used to use -1 with local fonts, but the webfonts from webtype.com must have different metrics. I don't know how to reduce the magnitude of the -6.
	$nav_links.css('margin-right', link_gutter_width);
	$nav_links.last().css('margin-right', 0).css('float', 'right');
}
function position_side_rollout(which) {
	var class_name = '.' + which;
	if ($('nav.strip').length && $('footer.main').length) {
		var top = $('nav.strip').offset().top + 80;
		var bottom = $('footer.main').offset().top;
		var height = bottom - top;
		var $reference_block = $('nav.strip ' + class_name + ' a');
		var right = $(window).width() - $reference_block.offset().left - $reference_block.width();
		$('section' + class_name + '.rollout.side').css('top', top).css('right', right);
	}
}
function resize_charts() {
	$('.chart.ring').each(function() {
		var $canvas = $(this).find('canvas');
		var w = $canvas.width();
		var w_retina = w*2;
		$canvas.height($canvas.width());
		$canvas.attr('width', w_retina).attr('height', w_retina);
	});
	vci_app.vertical_layout();
}
