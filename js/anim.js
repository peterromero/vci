(function() {
	var lastTime = 0;
	var vendors = ['ms', 'moz', 'webkit', 'o'];
	for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
		window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
		window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
	}
	if (!window.requestAnimationFrame) {
		window.requestAnimationFrame = function(callback, element) {
			var currTime = new Date().getTime();
			var timeToCall = Math.max(0, 16 - (currTime - lastTime));
			var id = window.setTimeout(function() { callback(currTime + timeToCall); },
			  timeToCall);
			lastTime = currTime + timeToCall;
			return id;
		};
	}
	if (!window.cancelAnimationFrame) {
		window.cancelAnimationFrame = function(id) {
			clearTimeout(id);
		};
	}
}());
var vci_anim = {
	easings: {
		// Easings transform any x from 0 to 1 on a linear scale to a value from 0 to 1 weighted along an easing curve.
		sqrt: function(x) {
			return Math.sqrt(x);
		},
		sqr: function(x) {
			return x * x;
		},
		ease_in_out: function(x) {
			return 3*x*x - 2*x*x*x;
		},
		ease_in_out_quint: function(x) {
			var y = x-.5;
			return (x-3.2*y*y*y*y*y-0.1)/0.8
		}
	},
	logo: {
		play_controller: '',
		rewind_controller: '',
		frame_height_px: 64,
		frame_count: 30,
		index_of_current_frame: 0,
		fps: 50,
		play: function() {
			var current_bg_pos = $('section.masthead.wide .logo').css('background-position').split(' ');
			var current_y_pos = parseInt(current_bg_pos[1].split('px'));
			vci_anim.logo.index_of_current_frame = -current_y_pos / vci_anim.logo.frame_height_px;
			if (vci_anim.logo.index_of_current_frame < vci_anim.logo.frame_count-1) {
				var new_y_pos = current_y_pos - vci_anim.logo.frame_height_px;
				$('section.masthead.wide .logo').css('background-position', '0px ' + new_y_pos + 'px');
			}
		},
		rewind: function() {
			var current_bg_pos = $('section.masthead.wide .logo').css('background-position').split(' ');
			var current_y_pos = parseInt(current_bg_pos[1].split('px'));
			if (current_y_pos) {
				vci_anim.logo.index_of_current_frame = -current_y_pos / vci_anim.logo.frame_height_px;
				var new_y_pos = current_y_pos + vci_anim.logo.frame_height_px;
				$('section.masthead.wide .logo').css('background-position', '0px ' + new_y_pos + 'px');
			}
			if (vci_anim.logo.index_of_current_frame==1) {
				window.clearInterval(vci_anim.logo.rewind_controller);
				vci_anim.logo.rewind_controller = "";
			}
		}
	},
	charts: {
		index_of_current_frame: 0,
		redraw_all: function() {
			var redraw_allowed = false;
			switch (vci_anim.charts.swing_open.animation_status) {
				case 'not_started': {
					redraw_allowed = true;
					frame_index = 0;
					break;
				}
				case 'complete': {
					redraw_allowed = true;
					frame_index = vci_anim.charts.swing_open.frame_count;
					break;
				}
			}
			if (redraw_allowed) {
				$('.chart.ring').each(function() {
					vci_anim.charts.swing_open._draw_frame($(this), frame_index);
				});
			}
		},
		swing_open: {
			frame_count: 300,
			animation_status: 'not_started',
			_draw_frame: function($chart, frame_index) {
				var $canvas = $chart.find('canvas');
				var chart_drawing_space_w = $canvas.attr('width');
				var chart_drawing_space_h = $canvas.attr('height');
				var chart_center_x = chart_drawing_space_w / 2;
				var chart_center_y = chart_drawing_space_h / 2;
				var c = $canvas.get(0).getContext('2d');
				var pct_anim_complete = vci_anim.easings.ease_in_out_quint(frame_index / vci_anim.charts.swing_open.frame_count);
				// Clear first
				c.clearRect(0,0,chart_drawing_space_w,chart_drawing_space_h);
				// Base circle
				c.strokeStyle = 'rgb(153,223,232)';
				c.lineWidth = 29 * pct_anim_complete + 1;
				c.beginPath();
				c.arc(chart_center_x, chart_center_y, chart_center_x - 15, 0, 2*Math.PI, false);
				c.stroke();
				// Dark arc
				c.strokeStyle = 'rgb(0,174,197)';
				c.beginPath();
				var start_angle = get_north_referent_angle_in_radians(0);
				var end_angle = get_north_referent_angle_in_radians($canvas.attr('data-value')*3.6*pct_anim_complete);
				c.arc(chart_center_x, chart_center_y, chart_center_x - 15, start_angle, end_angle, false);
				c.stroke();
				// Text
				if (window.innerWidth > 400) {
					big_font_size = 84;
					little_font_size = 28;
					line_height = 36;
					space_after = 12;
				} else {
					big_font_size = 72;
					little_font_size = 24;
					line_height = 28;
					space_after = 6;
				}
				c.font = big_font_size + 'px Lato';
				c.fillStyle = 'rgba(0,174,197,' + pct_anim_complete + ')';
				c.textAlign = 'center';
				c.textBaseline = 'bottom';
				var percentage_to_display = Math.floor($canvas.attr('data-value') * pct_anim_complete);
				c.fillText(percentage_to_display + '%',chart_center_x,chart_center_y);
				c.font = little_font_size + 'px Lato';
				var label = $canvas.attr('data-label').split('*');
				c.fillStyle = 'rgba(59,59,60,' + pct_anim_complete + ')';
				c.fillText(label[0],chart_center_x,chart_center_y+space_after+line_height);
				c.fillText(label[1],chart_center_x,chart_center_y+space_after+line_height*2);
			},
			play: function() {
				if (vci_anim.charts.index_of_current_frame <= vci_anim.charts.swing_open.frame_count) {
					vci_anim.charts.swing_open.animation_status = 'in_progress';
					var id = requestAnimationFrame(vci_anim.charts.swing_open.play);
					$('.chart.ring').each(function() {
						vci_anim.charts.swing_open._draw_frame($(this), vci_anim.charts.index_of_current_frame);
					});
					vci_anim.charts.index_of_current_frame++;
				} else {
					vci_anim.charts.swing_open.animation_status = 'complete';
				}
			}
		}
	}
};
$(window).scroll(function() {
	if (vci_app.has_charts) {
		if (vci_app.scroll_bottom() - vci_app.charts[0].height > vci_app.charts[0].top) {
			window.setTimeout(function() { // Wait a sec.
				vci_anim.charts.swing_open.play();
			}, 350);
		}
	}
});
$(window).load(function() {
	vci_anim.charts.redraw_all();
});
$(function() {
	$(window).resize(function() {
		vci_anim.charts.redraw_all();
	});
	$('section.masthead.wide .logo').hover(function() {
		if (vci_anim.logo.play_controller == "") {
			vci_anim.logo.play_controller = window.setInterval(vci_anim.logo.play, 1000/vci_anim.logo.fps);
		}
	}, function() {
		window.clearInterval(vci_anim.logo.play_controller);
		vci_anim.logo.play_controller = '';
		if (vci_anim.logo.rewind_controller == '') {
			vci_anim.logo.rewind_controller = window.setInterval(vci_anim.logo.rewind, 1000/vci_anim.logo.fps);
		}
	});
});
function get_north_referent_angle_in_radians(angle_in_degrees) {
	return (angle_in_degrees+270)/180*Math.PI;
}
