<?php
$FYL_GLOBALS = array();

$FYL_GLOBALS['server_os'] = 'Unix';

switch($FYL_GLOBALS['server_os']) {
	case 'Windows': {
		$FYL_GLOBALS['directory_separator'] = '\\';
		break;
	}
	case 'Unix': {
		$FYL_GLOBALS['directory_separator'] = '/';
		break;
	}
}

$FYL_GLOBALS['database_server'] = 'localhost';
$FYL_GLOBALS['database_name'] = 'freeyourlegs_2014';
$FYL_GLOBALS['database_username'] = 'freeyourlegs';
$FYL_GLOBALS['database_password'] = 'fyl2014';

$FYL_GLOBALS['mail_from'] = array('info@freeyourlegs.com' => 'Vein Care Institute');
$FYL_GLOBALS['mail_server'] = 'smtp.gmail.com';
$FYL_GLOBALS['mail_port'] = 465;
$FYL_GLOBALS['mail_security'] = 'ssl';
$FYL_GLOBALS['mail_username'] = 'admin@freeyourlegs.com';
$FYL_GLOBALS['mail_password'] = 'vciadminlogin';

$FYL_GLOBALS['mail_recipients'] = array(
	'drschuh@freeyourlegs.com' => 'William Schuh',
	'nili@freeyourlegs.com' => 'Nili',
	'lisa@freeyourlegs.com' => 'Lisa'
);

if (file_exists('config.local.php')) {
	include_once('config.local.php');
}
