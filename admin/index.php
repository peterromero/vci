<?php
	$absolute_path_to_root = $_SERVER['DOCUMENT_ROOT'] . '/';
	$relative_path_to_root = '../';
	$nav_highlighted_page = 'admin';
	$unique_page_id = 'admin';
	require_once("{$absolute_path_to_root}templating.php");
	open_document();
	echo_head_section(array(
		'title' => ':Administrative Tools',
		'is_admin_page' => true
	));
?>
<body class="page_<?php echo($unique_page_id); ?>">
	<?php echo_section('masthead'); ?>
	<section class="gray bottom_layer first">
		<div class="width_limiter">
			<div class="block_container">
				<h2 class="full">Administrative Tools</h2>
				<div class="half">
					<form id="rebuild_search_index" name="rebuild_search_index" method="post" data-action="/ajax/rebuild_search_index.php">
					<a href="#" class="button submit" data-form-id="rebuild_search_index" data-processed-text="Search Index Rebuilt">Rebuild Search Index</a>
					<p>Use this function to update the site's search engine after you have made changes to the text on one or more pages.</p>
					<p class="form_status_console" data-form-id="rebuild_search_index"></p>
				</div>
				<div class="half">
				</div>
			</div>
		</div>
	</section>
</body>
<?php close_document();
